/*
 * cview_file.h - header file for parsing vertex data files
 *
 * Jeffrey Biesiadecki, 9/21/2019
 */

#ifndef CVIEW_FILE_H_
#define CVIEW_FILE_H_

#include <stdint.h>

#include <GL/gl.h>

#include "cview.h"

/*-------------------------------------------------------------------------
 * declare public functions
 */

void cview_file_init(cview_data_t *cvdata_ptr, uint32_t nmax, GLfloat *v3_ptr);
int cview_file_read(cview_data_t *cvdata_ptr, const char *filename);
int cview_string_ends_with_cvf(const char *s);

#endif

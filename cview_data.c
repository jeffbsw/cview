/*
 * routines related to the data structure for holding vertices
 *
 * Jeffrey Biesiadecki, 9/28/2019
 */

#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "cview.h"

/*-------------------------------------------------------------------------
 * initialize data structure of vertices to render
 *
 * v3_ptr points to triplets of coordinates for each vertex,
 * and c3_ptr points to triplets of RGB color for each vertex.
 * c3_ptr can be specified as NULL to display with a solid color,
 * but v3_ptr must be non-NULL if the number of vertices is non-zero.
 *
 * the structure pointed to by cvdata_ptr keeps pointers to v3_ptr and c3_ptr,
 * so these arrays must be persistent as long as cvdata_ptr is in use.
 *
 * nmax - maximum number of X,Y,Z vertices
 * v3_ptr - points to already-allocated array of size at least 3*nmax
 * c3_ptr - points to already-allocated array of size at least 3*nmax
 */

void cview_data_init(cview_data_t *cvdata_ptr,
                     uint32_t nmax, GLfloat *v3_ptr, GLfloat *c3_ptr)
{
    assert(v3_ptr != NULL || nmax == 0U);

    if (cvdata_ptr != NULL) {
        memset(cvdata_ptr,'\0',sizeof(*cvdata_ptr));
        cvdata_ptr->n = 0U;
        cvdata_ptr->nmax = nmax;
        cvdata_ptr->v3_ptr = v3_ptr;
        cvdata_ptr->c3_ptr = c3_ptr;
    }
}

/*-------------------------------------------------------------------------
 * add a new vertex for rendering
 *
 * returns 0 if successful, -1 if an error (max number of vertices reached)
 */

int cview_data_add_xyz(cview_data_t *cvdata_ptr,
                       GLfloat vx, GLfloat vy, GLfloat vz)
{
    assert(cvdata_ptr != NULL);

    int status = -1;

    if (cvdata_ptr->n < cvdata_ptr->nmax) {
        GLfloat *v3_ptr = cvdata_ptr->v3_ptr + 3U*cvdata_ptr->n;
        *v3_ptr++ = vx;
        *v3_ptr++ = vy;
        *v3_ptr++ = vz;
        cvdata_ptr->n += 1U;

        status = 0;
    }

    return status;
}

/*-------------------------------------------------------------------------
 * calculate mean vertex location,
 * and a bounding box that encapsulates all vertices
 * with the mean vertex location at its center
 */

void cview_data_statistics(const cview_data_t *cvdata_ptr,
    cview_statistics_t *xstats_ptr,
    cview_statistics_t *ystats_ptr,
    cview_statistics_t *zstats_ptr)
{
    assert(cvdata_ptr != NULL);

    GLfloat xsum = 0.0f;
    GLfloat ysum = 0.0f;
    GLfloat zsum = 0.0f;
    GLfloat xsum2 = 0.0f;
    GLfloat ysum2 = 0.0f;
    GLfloat zsum2 = 0.0f;
    GLfloat xmin = 0.0f;
    GLfloat ymin = 0.0f;
    GLfloat zmin = 0.0f;
    GLfloat xmax = 0.0f;
    GLfloat ymax = 0.0f;
    GLfloat zmax = 0.0f;
    GLfloat xmean = 0.0f;
    GLfloat ymean = 0.0f;
    GLfloat zmean = 0.0f;
    GLfloat xstddev = 0.0f;
    GLfloat ystddev = 0.0f;
    GLfloat zstddev = 0.0f;
    GLfloat xmaxabsdev = 0.0f;
    GLfloat ymaxabsdev = 0.0f;
    GLfloat zmaxabsdev = 0.0f;
    uint32_t n = cvdata_ptr->n;

    /* find bounds and vertex mean */
    if (n > 0U) {
        GLfloat *v3_ptr = cvdata_ptr->v3_ptr;

        /* initialize to first sample */
        GLfloat vx = *v3_ptr++;
        GLfloat vy = *v3_ptr++;
        GLfloat vz = *v3_ptr++;
        xsum = vx;
        ysum = vy;
        zsum = vz;
        xsum2 = vx*vx;
        ysum2 = vy*vy;
        zsum2 = vz*vz;
        xmin = vx;
        ymin = vy;
        zmin = vz;
        xmax = vx;
        ymax = vy;
        zmax = vz;

        /* incorporate all other samples */
        uint32_t i;
        for (i = 1; i < n; i++) {
            GLfloat vx = *v3_ptr++;
            GLfloat vy = *v3_ptr++;
            GLfloat vz = *v3_ptr++;

            /* update sums for means and standard deviations */
            xsum += vx;
            ysum += vy;
            zsum += vz;
            xsum2 = vx*vx;
            ysum2 = vy*vy;
            zsum2 = vz*vz;

            /* update min values */
            if (vx < xmin) {
                xmin = vx;
            }
            if (vy < ymin) {
                ymin = vy;
            }
            if (vz < zmin) {
                zmin = vz;
            }

            /* update max values */
            if (vx > xmax) {
                xmax = vx;
            }
            if (vy > ymax) {
                ymax = vy;
            }
            if (vz > zmax) {
                zmax = vz;
            }
        }

        /* compute means */
        GLfloat rninv = 1.0f/((GLfloat)n);
        xmean = xsum*rninv;
        ymean = ysum*rninv;
        zmean = zsum*rninv;

        /* compute standard deviations */
        GLfloat rn1inv = (n <= 1U) ? 0.0f : 1.0f/((GLfloat)(n-1U));
        GLfloat xd2 = rn1inv*(xsum2*xsum2 - xsum*xsum);
        GLfloat yd2 = rn1inv*(ysum2*ysum2 - ysum*ysum);
        GLfloat zd2 = rn1inv*(zsum2*zsum2 - zsum*zsum);
        xstddev = (xd2 > 0.0f) ? sqrtf(xd2) : 0.0f;
        ystddev = (yd2 > 0.0f) ? sqrtf(yd2) : 0.0f;
        zstddev = (zd2 > 0.0f) ? sqrtf(zd2) : 0.0f;

        /* compute maximum absolute deviations */
        GLfloat xmean_xmin = xmean - xmin;
        GLfloat xmax_xmean = xmax - xmean;
        GLfloat ymean_ymin = ymean - ymin;
        GLfloat ymax_ymean = ymax - ymean;
        GLfloat zmean_zmin = zmean - zmin;
        GLfloat zmax_zmean = zmax - zmean;
        xmaxabsdev = (xmax_xmean > xmean_xmin) ? xmax_xmean : xmean_xmin;
        ymaxabsdev = (ymax_ymean > ymean_ymin) ? ymax_ymean : ymean_ymin;
        zmaxabsdev = (zmax_zmean > zmean_zmin) ? zmax_zmean : zmean_zmin;
    }

    /* set return values */
    if (xstats_ptr != NULL) {
        xstats_ptr->n = n;
        xstats_ptr->vmin = xmin;
        xstats_ptr->vmax = xmax;
        xstats_ptr->vmean = xmean;
        xstats_ptr->vstddev = xstddev;
        xstats_ptr->vmaxabsdev = xmaxabsdev;
    }
    if (ystats_ptr != NULL) {
        ystats_ptr->n = n;
        ystats_ptr->vmin = ymin;
        ystats_ptr->vmax = ymax;
        ystats_ptr->vmean = ymean;
        ystats_ptr->vstddev = ystddev;
        ystats_ptr->vmaxabsdev = ymaxabsdev;
    }
    if (zstats_ptr != NULL) {
        zstats_ptr->n = n;
        zstats_ptr->vmin = zmin;
        zstats_ptr->vmax = zmax;
        zstats_ptr->vmean = zmean;
        zstats_ptr->vstddev = zstddev;
        zstats_ptr->vmaxabsdev = zmaxabsdev;
    }
}

/*-------------------------------------------------------------------------
 * return value between y0 and y1,
 * linearly interpolating based on x between x0 and x1
 * and clipping to y0 and y1
 */

GLfloat cview_data_interp(GLfloat x,
                          GLfloat x0, GLfloat x1,
                          GLfloat y0, GLfloat y1)
{
    GLfloat f;
    GLfloat y;

    if (x0 < x1) {
        if (x <= x0) {
            y = y0;
        } else if (x >= x1) {
            y = y1;
        } else {
            f = (x-x0)/(x1-x0);
            y = y0 + f*(y1-y0);
        }
    } else if (x1 < x0) {
        if (x <= x1) {
            y = y1;
        } else if (x >= x0) {
            y = y0;
        } else {
            f = (x-x1)/(x0-x1);
            y = y1 + f*(y0-y1);
        }
    } else {
        y = 0.5f*(y1+y0);
    }

    return y;
}

/*-------------------------------------------------------------------------
 * helper to return Z range for a set of vertices
 */

static void cview_data_color_range(cview_data_t *cvdata_ptr,
    uint32_t first, uint32_t count,
    GLfloat *vmin_ptr, GLfloat *vmax_ptr)
{
    assert(cvdata_ptr != NULL);
    uint32_t n = cvdata_ptr->n;
    const GLfloat *v3_ptr = cvdata_ptr->v3_ptr;
    assert(n > 0U);
    assert(v3_ptr != NULL);

    assert(vmin_ptr != NULL);
    assert(vmax_ptr != NULL);

    assert(first < n);
    assert(first+count <= n);

    v3_ptr += 3U*first;

    v3_ptr += 2U; /* skip X and Y coordinates */
    GLfloat v = *v3_ptr++;
    GLfloat vmin = v;
    GLfloat vmax = v;

    uint32_t i;
    for (i = 1U; i < count; i++) {
        v3_ptr += 2U; /* skip X and Y coordinates */
        GLfloat v = *v3_ptr++;
        if (v < vmin) {
            vmin = v;
        } else if (v > vmax) {
            vmax = v;
        }
    }

    /* set return values */
    *vmin_ptr = vmin;
    *vmax_ptr = vmax;
}

/*-------------------------------------------------------------------------
 * helper to fill in color array for one set of vertices
 */

static void cview_data_color_assign(cview_data_t *cvdata_ptr,
    uint32_t first, uint32_t count,
    GLfloat zmin, GLfloat zmid, GLfloat zmax)
{
    assert(cvdata_ptr != NULL);
    uint32_t n = cvdata_ptr->n;
    const GLfloat *v3_ptr = cvdata_ptr->v3_ptr;
    GLfloat *c3_ptr = cvdata_ptr->c3_ptr;
    assert(n > 0U);
    assert(v3_ptr != NULL);
    assert(c3_ptr != NULL);

    assert(first < n);
    assert(first+count <= n);

    v3_ptr += 3U*first;
    c3_ptr += 3U*first;

    uint32_t i;
    for (i = 0U; i < count; i++) {
        v3_ptr += 2U; /* skip X and Y coordinates */
        GLfloat z = *v3_ptr++;
        *c3_ptr++ = cview_data_interp(z,zmid,zmax,1.0f,0.0f);
        *c3_ptr++ = cview_data_interp(z,zmin,zmid,0.0f,1.0f);
        *c3_ptr++ = 0.0f;
    }
}

/*-------------------------------------------------------------------------
 * fill in color array based on Z coordinates of vertices
 * whose color was not specifed
 */

void cview_data_color(cview_data_t *cvdata_ptr,
                      uint32_t ncvda, const cview_draw_arrays_t *cvda_ptr)
{
    assert(cvda_ptr != NULL || ncvda == 0U);

    /* make sure there are vertices and an array to hold color assignments */
    if (cvdata_ptr != NULL && cvdata_ptr->n > 0U &&
        cvdata_ptr->c3_ptr != NULL) {
        uint32_t n = cvdata_ptr->n;

        /* first, find range of z values */
        uint32_t vminmaxfound = 0U;
        GLfloat vmin = 0.0f;
        GLfloat vmax = 0.0f;
        if (ncvda == 0U) {
            /* vertices not split into sets, compute range over all */
            cview_data_color_range(cvdata_ptr,0U,n,&vmin,&vmax);
            vminmaxfound = 1U;
        } else {
            /* vertices split into sets, compute range for non-solid sets */
            uint32_t j;
            for (j = 0U; j < ncvda; j++) {
                const cview_draw_arrays_t *p = cvda_ptr+j;
                if (!p->use_solid_color && !p->use_assigned_colors) {
                    GLfloat smin = 0.0f;
                    GLfloat smax = 0.0f;
                    cview_data_color_range(cvdata_ptr,p->first,p->count,
                        &smin,&smax);
                    if (!vminmaxfound) {
                        vmin = smin;
                        vmax = smax;
                        vminmaxfound = 1U;
                    } else {
                        if (smin < vmin) {
                            vmin = smin;
                        }
                        if (smax > vmax) {
                            vmax = smax;
                        }
                    }
                }
            }
        }

        /* make sure some vertices need color assignment */
        if (vminmaxfound) {
            /* compute color map transition values */
            GLfloat dv = vmax-vmin;
            GLfloat zmin = vmin + CVIEW_CFG_ZMIN_FRACTION*dv;
            GLfloat zmax = vmin + CVIEW_CFG_ZMAX_FRACTION*dv;
            GLfloat zmid = vmin + CVIEW_CFG_ZMID_FRACTION*dv;

            if (ncvda == 0U) {
                /* vertices not split into sets, set color for all */
                cview_data_color_assign(cvdata_ptr,0U,n,zmin,zmid,zmax);
            } else {
                /* vertices split into sets, set color for non-solid sets */
                uint32_t j;
                for (j = 0U; j < ncvda; j++) {
                    const cview_draw_arrays_t *p = cvda_ptr+j;
                    if (!p->use_solid_color && !p->use_assigned_colors) {
                        cview_data_color_assign(cvdata_ptr,p->first,p->count,
                                                zmin,zmid,zmax);
                    }
                }
            }
        }
    }
}

/*-------------------------------------------------------------------------
 * generate a point cloud given f(x,y)
 *
 * at least useful for examples
 *
 * returns 0 if all points added successfully
 */

int cview_data_generate(cview_data_t *cvdata_ptr,
    uint32_t nx, uint32_t ny,
    GLfloat xmin, GLfloat xmax, GLfloat ymin, GLfloat ymax,
    cview_data_generate_fn_t fn, void *fn_client_data)
{
    int status = 0;

    assert(nx > 1U);
    assert(ny > 1U);
    assert(fn != 0);

    /* add each row */
    uint32_t i;
    for (i = 0U; i < nx; i++) {
        /* determine X coordinate */
        GLfloat xfrac = ((float)i)/((float)(nx-1U));
        GLfloat vx = xmin + xfrac*(xmax-xmin);

        /* add each column */
        uint32_t j;
        for (j = 0U; j < ny; j++) {
            /* determine Y coordinate */
            GLfloat yfrac = ((float)j)/((float)(ny-1U));
            GLfloat vy = ymin + yfrac*(ymax-ymin);

            /* determine Z coordinate */
            GLfloat vz = fn(vx,vy,fn_client_data);

            /* flip to OpenGL coordinates */
            status = cview_data_add_xyz(cvdata_ptr,vy,vx,-vz);
            if (status != 0) {
                break;
            }
        }
    }

    /* all done */
    return status;
}

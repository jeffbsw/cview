/*
 * cview_gl.c - OpenGL/GLU/GLUT routines for simple point cloud viewer
 *
 * using GLUT, which does not have client data pointers,
 * so state must be kept in global variables.
 *
 * also GLUT provides no way to return from its main loop,
 * so this code calls exit() when user wants to quit.
 *
 * Jeffrey Biesiadecki, 9/21/2019
 */

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glut.h>

#include "cview.h"

/*-------------------------------------------------------------------------
 * define private constants, types, and global variables
 */

/* epsilon value for checking cross product magnitude */
#define CVIEW_GL_XPROD_EPS (0.0001f)

/* graphics/windowing state */
typedef struct {
    /* render data and bounds */
    cview_data_t cvdata;                 /* vertex data */
    uint32_t ncvda;                      /* number of vertex sets to draw */
    const cview_draw_arrays_t *cvda_ptr; /* vertex set definitions */
    uint32_t ncvline;                    /* number of line segments to draw */
    const cview_line_t *cvline_ptr;      /* line segment endpoints and color */
    cview_statistics_t xstats;           /* statistics on X coordinates */
    cview_statistics_t ystats;           /* statistics on Y coordinates */
    cview_statistics_t zstats;           /* statistics on Z coordinates */
    GLfloat xlen; /* length along X of bounding box centered at mean */
    GLfloat ylen; /* length along Y of bounding box centered at mean */
    GLfloat zlen; /* length along Z of bounding box centered at mean */
    GLfloat rlen; /* max of xlen, ylen, and zlen */

    /* window state */
    int window;         /* id of created window */
    int window_w;       /* window width, pixels */
    int window_h;       /* window height, pixels */

    /* projection state */
    double frustum_base; /* base frustum dimension */
    double frustum_near; /* near clipping plane */
    double frustum_far;  /* far clipping plane */

    /* mouse state */
    int mouse_rotating;    /* whether or not rotation is active */
    int mouse_translating; /* whether or not translation is active */
    int mouse_x;           /* previous pixel coordinate, X */
    int mouse_y;           /* previous pixel coordinate, Y */

    /* camera state */
    GLdouble camera_eye_x;
    GLdouble camera_eye_y;
    GLdouble camera_eye_z;
    GLdouble camera_center_x;
    GLdouble camera_center_y;
    GLdouble camera_center_z;
    GLdouble camera_up_x;
    GLdouble camera_up_y;
    GLdouble camera_up_z;

    /* rotation state */
    uint32_t rot_center_n; /* 0..ncvline-1 for start of line, else mean */
    GLfloat rot_center_x; /* center of rotation, X coordinate */
    GLfloat rot_center_y; /* center of rotation, Y coordinate */
    GLfloat rot_center_z; /* center of rotation, Z coordinate */
    int32_t rot_x; /* current rotation about X axis, degrees */
    int32_t rot_y; /* current rotation about Y axis, degrees */
    int32_t rot_z; /* current rotation about Z axis, degrees */

    /* translation state */
    GLfloat d_x; /* current translation along X axis */
    GLfloat d_y; /* current translation along Y axis */
    GLfloat d_z; /* current translation along Z axis */

    /* visualization state */
    cview_line_style_t line_style; /* how to render line segments */
    uint16_t point_size; /* point size to render vertices */
    uint16_t line_width; /* width of axes and line segments */
    uint16_t axis_pct;   /* axis length as pct of bounding box, 0 turns off */
    uint16_t wire_swap;  /* swap GL_LINE_LOOP and GL_TRIANGLE_FAN */
} cview_gl_t;

/* graphics/windowing state */
cview_gl_t cview_gl;

/*-------------------------------------------------------------------------
 * return cross product of two vectors
 */

void cview_gl_xprod(GLfloat *cx_ptr, GLfloat *cy_ptr, GLfloat *cz_ptr,
                    GLfloat ax, GLfloat ay, GLfloat az,
                    GLfloat bx, GLfloat by, GLfloat bz)
{
    if (cx_ptr != NULL) {
        *cx_ptr = ay*bz-az*by;
    }
    if (cy_ptr != NULL) {
        *cy_ptr = az*bx-ax*bz;
    }
    if (cz_ptr != NULL) {
        *cz_ptr = ax*by-ay*bx;
    }
}

/*-------------------------------------------------------------------------
 * print help/key mapping to file pointer (or stderr if NULL specified)
 */

static void cview_gl_print_keymap(FILE *fp)
{
    if (fp == NULL) {
        fp = stderr;
    }

    (void)fprintf(fp,
        "Keyboard mapping, when 3D view window has focus:\n"
        "F1/?      get this help\n"
        "HOME      reset view back to initial view\n"
        "a/A       increase/decrease axis display, also turns on/off\n"
        "c/C       cycle center between line segment start and vertex mean\n"
        "l/L       increase/decrease line segment width\n"
        "m/M       increase/decrease line/plane rendering style\n"
        "p/P       increase/decrease point cloud vertex size\n"
        "q/Q/ESC   quit program\n"
        "w/W       swap wire and fill frame rendering modes\n"
        "z/Z       move points towards/away from camera\n"
        "LMB       hold down to translate points and line segments\n"
        "RMB       hold down to rotate points and line segments\n"
        "ARROWS    translate points and line segments\n"
        "PGUP/DN   rotate points and line segments CCW/CW\n"
        "\n");
}

/*-------------------------------------------------------------------------
 * set center of rotation
 */

static void cview_gl_rot_center(void)
{
    if (cview_gl.rot_center_n < cview_gl.ncvline) {
        const cview_line_t *cvline_ptr =
            cview_gl.cvline_ptr+cview_gl.rot_center_n;
        cview_gl.rot_center_x = cvline_ptr->x0;
        cview_gl.rot_center_y = cvline_ptr->y0;
        cview_gl.rot_center_z = cvline_ptr->z0;
    } else {
        cview_gl.rot_center_x = cview_gl.xstats.vmean;
        cview_gl.rot_center_y = cview_gl.ystats.vmean;
        cview_gl.rot_center_z = cview_gl.zstats.vmean;
    }
    cview_gl.d_x = cview_gl.xstats.vmean - cview_gl.rot_center_x;
    cview_gl.d_y = cview_gl.ystats.vmean - cview_gl.rot_center_y;
    cview_gl.d_z = cview_gl.zstats.vmean - cview_gl.rot_center_z;
}

/*-------------------------------------------------------------------------
 * reset graphics state
 */

static void cview_gl_reset(void)
{
    /* camera pointed at vertex mean along OpenGL -Z, up along OpenGL +Y */
    cview_gl.camera_eye_x = cview_gl.xstats.vmean;
    cview_gl.camera_eye_y = cview_gl.ystats.vmean;
    cview_gl.camera_eye_z =
        cview_gl.zstats.vmean+CVIEW_CFG_ZVFACTOR*cview_gl.rlen;
    cview_gl.camera_center_x = cview_gl.xstats.vmean;
    cview_gl.camera_center_y = cview_gl.ystats.vmean;
    cview_gl.camera_center_z = cview_gl.zstats.vmean;
    cview_gl.camera_up_x = 0.0;
    cview_gl.camera_up_y = 1.0;
    cview_gl.camera_up_z = 0.0;

    /* no rotation */
    cview_gl.rot_x = 0;
    cview_gl.rot_y = 0;
    cview_gl.rot_z = 0;

    /* no translation */
    cview_gl.d_x = 0.0f; /* actually, */
    cview_gl.d_y = 0.0f; /* this is overridden */
    cview_gl.d_z = 0.0f; /* in the call to cview_gl_rot_center() below */

    /* specify center of rotation */
    cview_gl.rot_center_n = cview_gl.ncvline; /* about mean */
    cview_gl_rot_center();
}

/*-------------------------------------------------------------------------
 * graphics state initialization
 * this must be called prior to opening display window
 */

static void cview_gl_init(const cview_data_t *cvdata_ptr,
                          uint32_t ncvda, const cview_draw_arrays_t *cvda_ptr,
                          uint32_t ncvline, const cview_line_t *cvline_ptr)
{
    assert(cvdata_ptr != NULL);

    /* ensure everything is initialized */
    memset(&cview_gl,'\0',sizeof(cview_gl));

    /* make a shallow copy of vertex pointers and counters */
    cview_gl.cvdata = *cvdata_ptr;

    /* and just remember pointer to array of vertex sets */
    cview_gl.ncvda = ncvda;
    cview_gl.cvda_ptr = cvda_ptr;

    /* and just remember pointer to array of line segments */
    cview_gl.ncvline = ncvline;
    cview_gl.cvline_ptr = cvline_ptr;

    /* find vertex mean and bounds */
    cview_data_statistics(&cview_gl.cvdata,
        &cview_gl.xstats,&cview_gl.ystats,&cview_gl.zstats);
    cview_gl.xlen = 2.0f*cview_gl.xstats.vmaxabsdev;
    if (cview_gl.xlen < CVIEW_CFG_RMIN) {
        cview_gl.xlen = CVIEW_CFG_RMIN;
    }
    cview_gl.ylen = 2.0f*cview_gl.ystats.vmaxabsdev;
    if (cview_gl.ylen < CVIEW_CFG_RMIN) {
        cview_gl.ylen = CVIEW_CFG_RMIN;
    }
    cview_gl.zlen = 2.0f*cview_gl.zstats.vmaxabsdev;
    if (cview_gl.zlen < CVIEW_CFG_RMIN) {
        cview_gl.zlen = CVIEW_CFG_RMIN;
    }
    cview_gl.rlen = cview_gl.xlen;
    if (cview_gl.ylen > cview_gl.rlen) {
        cview_gl.rlen = cview_gl.ylen;
    }
    if (cview_gl.zlen > cview_gl.rlen) {
        cview_gl.rlen = cview_gl.zlen;
    }

    /* compute vertex colors based on Z coordinates */
    cview_data_color(&cview_gl.cvdata,cview_gl.ncvda,cview_gl.cvda_ptr);

    /* window dimensions */
    cview_gl.window_w = CVIEW_CFG_WIN_W;
    cview_gl.window_h = CVIEW_CFG_WIN_H;

    /* base frustum on vertex bounds */
    cview_gl.frustum_base = CVIEW_CFG_FRUSTUM_BASE_FACTOR*cview_gl.rlen;
    cview_gl.frustum_near = CVIEW_CFG_FRUSTUM_NEAR_FACTOR*cview_gl.rlen;
    cview_gl.frustum_far = CVIEW_CFG_FRUSTUM_FAR_FACTOR*cview_gl.rlen;

    /* initialize display options */
    cview_gl.line_style = CVIEW_LINE_STYLE_PLANE;
    cview_gl.point_size = (cview_gl.cvdata.n <= CVIEW_CFG_POINT_SIZE_NFEW) ?
        CVIEW_CFG_POINT_SIZE_DEFAULT : CVIEW_CFG_POINT_SIZE_MIN;
    cview_gl.line_width = CVIEW_CFG_LINE_WIDTH_DEFAULT;
    cview_gl.axis_pct = CVIEW_CFG_AXIS_PCT_DEFAULT;
    cview_gl.wire_swap = 0U;

    /* set initial viewing state */
    cview_gl_reset();
}

/*-------------------------------------------------------------------------
 * GLUT exit, as cleanly as possible
 */

static void cview_gl_exit(void)
{
    /* close the one window that has been opened */
    glutDestroyWindow(cview_gl.window);
    cview_gl.window = 0;
    cview_gl.window_w = 0;
    cview_gl.window_h = 0;

    /* apparently no way to break GLUT's main loop */
    exit(0);
}

/*-------------------------------------------------------------------------
 * display axis centered at vertex average
 */

static void cview_gl_display_axis(void)
{
    GLfloat r = (0.01f*cview_gl.axis_pct)*cview_gl.rlen;

    glLineWidth(cview_gl.line_width);

    glColor3f(1.0f,0.0f,0.0f);
    glBegin(GL_LINES);
    glVertex3f(cview_gl.xstats.vmean,
               cview_gl.ystats.vmean,
               cview_gl.zstats.vmean);
    glVertex3f(cview_gl.xstats.vmean,
               cview_gl.ystats.vmean+r,
               cview_gl.zstats.vmean);
    glEnd();

    glColor3f(0.0f,1.0f,0.0f);
    glBegin(GL_LINES);
    glVertex3f(cview_gl.xstats.vmean,
               cview_gl.ystats.vmean,
               cview_gl.zstats.vmean);
    glVertex3f(cview_gl.xstats.vmean+r,
               cview_gl.ystats.vmean,
               cview_gl.zstats.vmean);
    glEnd();

    glColor3f(0.0f,0.0f,1.0f);
    glBegin(GL_LINES);
    glVertex3f(cview_gl.xstats.vmean,
               cview_gl.ystats.vmean,
               cview_gl.zstats.vmean);
    glVertex3f(cview_gl.xstats.vmean,
               cview_gl.ystats.vmean,
               cview_gl.zstats.vmean-r);
    glEnd();
}

/*-------------------------------------------------------------------------
 * display lines
 */

static void cview_gl_display_lines(void)
{
    assert(cview_gl.cvline_ptr != NULL || cview_gl.ncvline == 0U);

    if (cview_gl.line_style == CVIEW_LINE_STYLE_NONE) {
        return;
    }

    glLineWidth(cview_gl.line_width);

    uint32_t i;
    uint32_t ncvline = cview_gl.ncvline;
    const cview_line_t *cvline_ptr = cview_gl.cvline_ptr;
    for (i = 0U; i < ncvline; i++) {
        /* specify the color */
        glColor3f(cvline_ptr->r,cvline_ptr->g,cvline_ptr->b);

        /* draw the current line segment */
        glBegin(GL_LINES);
        glVertex3f(cvline_ptr->x0,cvline_ptr->y0,cvline_ptr->z0);
        glVertex3f(cvline_ptr->x1,cvline_ptr->y1,cvline_ptr->z1);
        glEnd();

        /* draw plane at segment starting point, if needed */
        if (cvline_ptr->plane &&
            cview_gl.line_style == CVIEW_LINE_STYLE_PLANE) {
            /* declare variables for plane at line segment start */
            GLfloat px0;
            GLfloat px1;
            GLfloat px2;
            GLfloat px3;
            GLfloat py0;
            GLfloat py1;
            GLfloat py2;
            GLfloat py3;
            GLfloat pz0;
            GLfloat pz1;
            GLfloat pz2;
            GLfloat pz3;
            GLfloat px;
            GLfloat py;
            GLfloat pz;
            GLfloat dx = cvline_ptr->x1 - cvline_ptr->x0;
            GLfloat dy = cvline_ptr->y1 - cvline_ptr->y0;
            GLfloat dz = cvline_ptr->z1 - cvline_ptr->z0;
            GLfloat r =
                (0.01f*CVIEW_CFG_PLANE_DIAG_PCT)*sqrtf(dx*dx+dy*dy+dz*dz);
            GLfloat p;
            GLfloat s;

            /* coords for plane based on cross product with X (or Y) axis */
            px = 0.0f;
            py = 0.0f;
            pz = 0.0f;
            cview_gl_xprod(&px,&py,&pz,1.0f,0.0f,0.0f,dx,dy,dz);
            p = sqrtf(px*px+py*py+pz*pz);
            if (p <= CVIEW_GL_XPROD_EPS) {
                px = 0.0f;
                py = 0.0f;
                pz = 0.0f;
                cview_gl_xprod(&px,&py,&pz,0.0f,1.0f,0.0f,dx,dy,dz);
                p = sqrtf(px*px+py*py+pz*pz);
            }
            s = (p > CVIEW_GL_XPROD_EPS) ? r/p : 0.0f; /* could assert */
            px *= s;
            py *= s;
            pz *= s;
            px0 = cvline_ptr->x0+px;
            py0 = cvline_ptr->y0+py;
            pz0 = cvline_ptr->z0+pz;
            px2 = cvline_ptr->x0-px;
            py2 = cvline_ptr->y0-py;
            pz2 = cvline_ptr->z0-pz;

            /* coords for plane based on cross product with p0->p2 */
            px = 0.0f;
            py = 0.0f;
            pz = 0.0f;
            cview_gl_xprod(&px,&py,&pz,px2-px0,py2-py0,pz2-pz0,dx,dy,dz);
            p = sqrtf(px*px+py*py+pz*pz);
            s = (p > CVIEW_GL_XPROD_EPS) ? r/p : 0.0f; /* could assert */
            px *= s;
            py *= s;
            pz *= s;
            px1 = cvline_ptr->x0+px;
            py1 = cvline_ptr->y0+py;
            pz1 = cvline_ptr->z0+pz;
            px3 = cvline_ptr->x0-px;
            py3 = cvline_ptr->y0-py;
            pz3 = cvline_ptr->z0-pz;

            /* specify the color */
            glColor3f((0.01f*CVIEW_CFG_PLANE_COLOR_PCT)*cvline_ptr->r,
                      (0.01f*CVIEW_CFG_PLANE_COLOR_PCT)*cvline_ptr->g,
                      (0.01f*CVIEW_CFG_PLANE_COLOR_PCT)*cvline_ptr->b);

            /* draw the plane line segment start */
            glBegin(GL_QUADS);
            glVertex3f(px0,py0,pz0);
            glVertex3f(px1,py1,pz1);
            glVertex3f(px2,py2,pz2);
            glVertex3f(px3,py3,pz3);
            glEnd();
        }

        /* advance to the next segment */
        cvline_ptr += 1;
    }
}

/*-------------------------------------------------------------------------
 * display dot at center of rotation
 */

static void cview_gl_display_rot_center(void)
{
    /* see if this display should be skipped */
    uint8_t skip = 0U;
    if (cview_gl.rot_center_n < cview_gl.ncvline) {
        skip = (cview_gl.line_style == CVIEW_LINE_STYLE_NONE);
    } else {
        skip = (cview_gl.axis_pct == 0U);
    }
    if (skip) {
        return;
    }

    /* draw dot at center of rotation */
    /* note: base on line_width since dot is at axis or segment origin */
    glPointSize(cview_gl.line_width+CVIEW_CFG_ROT_CENTER_POINT_SIZE_OFFSET);
    glColor3f(CVIEW_CFG_ROT_CENTER_COLOR_R,
              CVIEW_CFG_ROT_CENTER_COLOR_G,
              CVIEW_CFG_ROT_CENTER_COLOR_B);
    glBegin(GL_POINTS);
    glVertex3f(cview_gl.rot_center_x,
               cview_gl.rot_center_y,
               cview_gl.rot_center_z);
    glEnd();
}

/*-------------------------------------------------------------------------
 * display all vertex sets
 */

static void cview_gl_display_vertices(void)
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glPointSize(cview_gl.point_size);

    if (cview_gl.ncvda == 0U) {
        /* vertices not split into sets, make single call to draw arrays */

        /* setup color */
        if (cview_gl.cvdata.c3_ptr != NULL) {
            glEnableClientState(GL_COLOR_ARRAY);
        } else {
            glDisableClientState(GL_COLOR_ARRAY);
            glColor3f(CVIEW_CFG_POINTS_COLOR_R,
                      CVIEW_CFG_POINTS_COLOR_G,
                      CVIEW_CFG_POINTS_COLOR_B);
        }

        /* display vertices */
        glDrawArrays(GL_POINTS,0,cview_gl.cvdata.n);
    } else {
        /* vertices split into sets, call draw arrays for each set */
        uint32_t i;
        for (i = 0U; i < cview_gl.ncvda; i++) {
            /* setup color for this set */
            const cview_draw_arrays_t *cvda_ptr = cview_gl.cvda_ptr+i;
            if (cvda_ptr->use_solid_color) {
                glDisableClientState(GL_COLOR_ARRAY);
                glColor3f(cvda_ptr->solid_color_r,
                          cvda_ptr->solid_color_g,
                          cvda_ptr->solid_color_b);
            } else if (cview_gl.cvdata.c3_ptr != NULL) {
                glEnableClientState(GL_COLOR_ARRAY);
            } else {
                glDisableClientState(GL_COLOR_ARRAY);
                glColor3f(CVIEW_CFG_POINTS_COLOR_R,
                          CVIEW_CFG_POINTS_COLOR_G,
                          CVIEW_CFG_POINTS_COLOR_B);
            }

            /* determine settings for this vertex set */
            uint32_t mode = cvda_ptr->mode;
            uint32_t first = cvda_ptr->first;
            uint32_t count = cvda_ptr->count;
            uint32_t polygon = cvda_ptr->polygon;
            uint32_t prism = cvda_ptr->prism;

            /* only some modes work as prisms */
            /* like GL_POINTS and GL_LINES do not make sense as prisms */
            prism = prism && polygon >= 3U &&
                (mode == GL_LINE_LOOP ||
                 mode == GL_TRIANGLES ||
                 mode == GL_TRIANGLE_FAN);

            /* see if flipping wire and fill modes */
            if (cview_gl.wire_swap) {
                if (mode == GL_LINE_LOOP) {
                    mode = (polygon == 3U) ? GL_TRIANGLES : GL_TRIANGLE_FAN;
                } else if (mode == GL_TRIANGLES) {
                    mode = GL_LINE_LOOP;
                    polygon = 3U;
                } else if (mode == GL_TRIANGLE_FAN) {
                    mode = GL_LINE_LOOP;
                }
            }

            /* make sure polygon is sensible/optimal */
            if (!prism) {
                if (mode == GL_TRIANGLE_FAN && polygon == 3U) {
                    mode = GL_TRIANGLES;
                }
                if (mode == GL_POINTS ||
                    mode == GL_LINES ||
                    mode == GL_TRIANGLES) {
                    polygon = 0U;
                }
            }

            /* display vertices for this set */
            if (polygon < 3U) {
                glDrawArrays(mode,first,count);
            } else if (!prism) {
                uint32_t j;
                for (j = first; j+polygon-1 < first+count; j += polygon) {
                    glDrawArrays(mode,j,polygon);
                }
            } else if (mode == GL_LINE_LOOP) {
                uint32_t j;
                uint32_t polygon2 = 2U*polygon;
                for (j = first; j+polygon2-1 < first+count; j += polygon2) {
                    /* top and bottom of prism */
                    glDrawArrays(GL_LINE_LOOP,j,polygon);
                    glDrawArrays(GL_LINE_LOOP,j+polygon,polygon);

                    /* prism edges */
                    GLuint indices[polygon2];
                    GLuint *indices_ptr = indices;
                    uint32_t k;
                    for (k = 0U; k < polygon; k++) {
                        *indices_ptr++ = j+k;
                        *indices_ptr++ = j+k+polygon;
                    }
                    glDrawElements(GL_LINES,polygon2,GL_UNSIGNED_INT,indices);
                }
            } else {
                uint32_t j;
                uint32_t polygon2 = 2U*polygon;
                for (j = first; j+polygon2-1 < first+count; j += polygon2) {
                    /* top and bottom of prism */
                    glDrawArrays(mode,j,polygon);       /* top */
                    glDrawArrays(mode,j+polygon,polygon); /* bottom */

                    /* prism sides */
                    GLuint indices[polygon2+2U];
                    GLuint *indices_ptr = indices;
                    uint32_t k;
                    for (k = 0U; k < polygon; k++) {    /* sides */
                        *indices_ptr++ = j+k;
                        *indices_ptr++ = j+k+polygon;
                    }
                    *indices_ptr++ = j;
                    *indices_ptr++ = j+polygon;
                    glDrawElements(GL_TRIANGLE_STRIP,polygon2+2U,
                                   GL_UNSIGNED_INT,indices);
                }
            }
        }
    }
}

/*-------------------------------------------------------------------------
 * GLUT callback for displaying scene
 */

static void cview_gl_display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    /* set camera location and orientation */
    gluLookAt(cview_gl.camera_eye_x,
              cview_gl.camera_eye_y,
              cview_gl.camera_eye_z,
              cview_gl.camera_center_x,
              cview_gl.camera_center_y,
              cview_gl.camera_center_z,
              cview_gl.camera_up_x,
              cview_gl.camera_up_y,
              cview_gl.camera_up_z);

    /* apply translations and rotations */
    glTranslatef(cview_gl.rot_center_x,
                 cview_gl.rot_center_y,
                 cview_gl.rot_center_z);
    glTranslatef(cview_gl.d_x,cview_gl.d_y,cview_gl.d_z);
    glRotatef(cview_gl.rot_z,0.0f,0.0f,1.0f);
    glRotatef(cview_gl.rot_y,0.0f,1.0f,0.0f);
    glRotatef(cview_gl.rot_x,1.0f,0.0f,0.0f);
    glTranslatef(-cview_gl.rot_center_x,
                 -cview_gl.rot_center_y,
                 -cview_gl.rot_center_z);

    /* draw axis, unless length cycled to 0 */
    if (cview_gl.axis_pct > 0U) {
        cview_gl_display_axis();
    }

    /* draw line segments */
    cview_gl_display_lines();

    /* draw vertices */
    cview_gl_display_vertices();

    /* draw dot at center of rotation */
    cview_gl_display_rot_center();

    /* actually display scene */
    glutSwapBuffers();
}

/*-------------------------------------------------------------------------
 * GLUT callback for special key press
 */

static void cview_gl_special(int key, int x, int y)
{
    (void)x;
    (void)y;

    /* perform action depending on which key was pressed */
    switch (key) {
        case GLUT_KEY_F1:
            cview_gl_print_keymap(NULL);
            break;
        case GLUT_KEY_LEFT:
            /* move point cloud to the left on screen */
            cview_gl.d_x -= (0.01f*CVIEW_CFG_TRANSLATE_PCT_KEY)*cview_gl.rlen;
            break;
        case GLUT_KEY_RIGHT:
            /* move point cloud to the right on screen */
            cview_gl.d_x += (0.01f*CVIEW_CFG_TRANSLATE_PCT_KEY)*cview_gl.rlen;
            break;
        case GLUT_KEY_DOWN:
            /* move point cloud down on screen */
            cview_gl.d_y -= (0.01f*CVIEW_CFG_TRANSLATE_PCT_KEY)*cview_gl.rlen;
            break;
        case GLUT_KEY_UP:
            /* move point cloud up on script */
            cview_gl.d_y += (0.01f*CVIEW_CFG_TRANSLATE_PCT_KEY)*cview_gl.rlen;
            break;
        case GLUT_KEY_PAGE_UP:
            /* rotate point cloud counter-clockwise on screen */
            cview_gl.rot_z += CVIEW_CFG_ROTATE_DEG_KEY;
            break;
        case GLUT_KEY_PAGE_DOWN:
            /* rotate point cloud clockwise on screen */
            cview_gl.rot_z -= CVIEW_CFG_ROTATE_DEG_KEY;
            break;
        case GLUT_KEY_HOME:
            /* reset view of vertices */
            cview_gl_reset();
            break;
        default:
            /* nothing */
            break;
    }

    /* refresh display */
    glutPostRedisplay();
}

/*-------------------------------------------------------------------------
 * GLUT callback for standard key press
 */

static void cview_gl_keyboard(unsigned char key, int x, int y)
{
    (void)x;
    (void)y;

    /* perform action depending on which key was pressed */
    switch (key) {
        case '?':
            cview_gl_print_keymap(NULL);
            break;
        case 'A':
            /* decrease axis size, eventually turning off axis display */
            if (cview_gl.axis_pct >=
                CVIEW_CFG_AXIS_PCT_MIN+CVIEW_CFG_AXIS_PCT_DELTA) {
                cview_gl.axis_pct -= CVIEW_CFG_AXIS_PCT_DELTA;
            } else {
                cview_gl.axis_pct = 0U;
            }
            break;
        case 'a':
            /* increase axis size, or turn on axis display */
            if (cview_gl.axis_pct == 0U) {
                cview_gl.axis_pct = CVIEW_CFG_AXIS_PCT_MIN;
            } else if (cview_gl.axis_pct+CVIEW_CFG_AXIS_PCT_DELTA <=
                       CVIEW_CFG_AXIS_PCT_MAX) {
                cview_gl.axis_pct += CVIEW_CFG_AXIS_PCT_DELTA;
            }
            break;
        case 'C':
            /* cycle through line segments for center of rotation */
            if (cview_gl.rot_center_n > 0U) {
                cview_gl.rot_center_n -= 1U;
            } else {
                cview_gl.rot_center_n = cview_gl.ncvline; /* about mean */
            }
            cview_gl_rot_center();
            break;
        case 'c':
            /* cycle through line segments for center of rotation */
            if (cview_gl.rot_center_n < cview_gl.ncvline) {
                cview_gl.rot_center_n += 1U;
            } else {
                cview_gl.rot_center_n = 0U;
            }
            cview_gl_rot_center();
            break;
        case 'L':
            /* decrease line width */
            if (cview_gl.line_width >=
                CVIEW_CFG_LINE_WIDTH_MIN+CVIEW_CFG_LINE_WIDTH_DELTA) {
                cview_gl.line_width -= CVIEW_CFG_LINE_WIDTH_DELTA;
            }
            break;
        case 'l':
            /* increase line width */
            if (cview_gl.line_width+CVIEW_CFG_LINE_WIDTH_DELTA <=
                CVIEW_CFG_LINE_WIDTH_MAX) {
                cview_gl.line_width += CVIEW_CFG_LINE_WIDTH_DELTA;
            }
            break;
        case 'M':
            /* decrease line/plane display mode */
            if (cview_gl.line_style > 0) {
                cview_gl.line_style -= 1;
            } else {
                cview_gl.line_style = CVIEW_NUM_LINE_STYLE-1;
            }
            break;
        case 'm':
            /* increase line/plane display mode */
            if (cview_gl.line_style < CVIEW_NUM_LINE_STYLE-1) {
                cview_gl.line_style += 1;
            } else {
                cview_gl.line_style = 0;
            }
            break;
        case 'P':
            /* decrease point size */
            if (cview_gl.point_size >=
                CVIEW_CFG_POINT_SIZE_MIN+CVIEW_CFG_POINT_SIZE_DELTA) {
                cview_gl.point_size -= CVIEW_CFG_POINT_SIZE_DELTA;
            }
            break;
        case 'p':
            /* increase point size */
            if (cview_gl.point_size+CVIEW_CFG_POINT_SIZE_DELTA <=
                CVIEW_CFG_POINT_SIZE_MAX) {
                cview_gl.point_size += CVIEW_CFG_POINT_SIZE_DELTA;
            }
            break;
        case 'Q':
        case 'q':
        case 27: /* escape */
            /* quit program */
            cview_gl_exit();
            break;
        case 'W':
        case 'w':
            cview_gl.wire_swap = !cview_gl.wire_swap;
            break;
        case 'z':
        {
            /* move point cloud towards camera */
            GLfloat dzmax =
                (cview_gl.camera_eye_z - cview_gl.camera_center_z)
                - CVIEW_CFG_ZNEAR_FRACTION*cview_gl.frustum_near;
            cview_gl.d_z += 0.01f*CVIEW_CFG_TRANSLATE_PCT_KEY*cview_gl.rlen;
            if (cview_gl.d_z > dzmax) {
                cview_gl.d_z = dzmax;
            }
            break;
        }
        case 'Z':
        {
            /* move point cloud away from camera */
            GLfloat dzmin =
                (cview_gl.camera_eye_z - cview_gl.camera_center_z)
                - CVIEW_CFG_ZFAR_FRACTION*cview_gl.frustum_far;
            cview_gl.d_z -= 0.01f*CVIEW_CFG_TRANSLATE_PCT_KEY*cview_gl.rlen;
            if (cview_gl.d_z < dzmin) {
                cview_gl.d_z = dzmin;
            }
            break;
        }
        default:
            /* nothing */
            break;
    }

    /* refresh display */
    glutPostRedisplay();
}

/*-------------------------------------------------------------------------
 * GLUT callback for mouse button state change
 */

static void cview_gl_mouse(int button, int state, int x, int y)
{
    /* remember mouse position if starting rotation */
    if (button == GLUT_RIGHT_BUTTON && !cview_gl.mouse_translating) {
        cview_gl.mouse_rotating = (state == GLUT_DOWN);
        cview_gl.mouse_x = x;
        cview_gl.mouse_y = y;
    }

    /* remember mouse position if starting translation */
    if (button == GLUT_LEFT_BUTTON && !cview_gl.mouse_rotating) {
        cview_gl.mouse_translating = (state == GLUT_DOWN);
        cview_gl.mouse_x = x;
        cview_gl.mouse_y = y;
    }

    /* see if scroll wheel or trackpad is being used, and zoom if so */
    if (state == GLUT_DOWN) {
        if (button == 3) { /* apparently, most reliable means for scroll up */
            /* move point cloud towards camera */
            GLfloat dzmax =
                (cview_gl.camera_eye_z - cview_gl.camera_center_z)
                - CVIEW_CFG_ZNEAR_FRACTION*cview_gl.frustum_near;
            cview_gl.d_z +=
                0.01f*0.5f*CVIEW_CFG_TRANSLATE_PCT_KEY*cview_gl.rlen;
            if (cview_gl.d_z > dzmax) {
                cview_gl.d_z = dzmax;
            }

            /* refresh display */
            glutPostRedisplay();
        } else if (button == 4) { /* and scroll down */
            /* move point cloud away from camera */
            GLfloat dzmin =
                (cview_gl.camera_eye_z - cview_gl.camera_center_z)
                - CVIEW_CFG_ZFAR_FRACTION*cview_gl.frustum_far;
            cview_gl.d_z -=
                0.01f*0.5f*CVIEW_CFG_TRANSLATE_PCT_KEY*cview_gl.rlen;
            if (cview_gl.d_z < dzmin) {
                cview_gl.d_z = dzmin;
            }

            /* refresh display */
            glutPostRedisplay();
        }
    }
}

/*-------------------------------------------------------------------------
 * GLUT callback for mouse motion while a button is held down
 */

static void cview_gl_motion(int x, int y)
{
    if (cview_gl.mouse_rotating || cview_gl.mouse_translating) {
        int dx = x - cview_gl.mouse_x;
        int dy = y - cview_gl.mouse_y;
        cview_gl.mouse_x = x;
        cview_gl.mouse_y = y;

        /* update rotation */
        if (cview_gl.mouse_rotating) {
            cview_gl.rot_x += (int)(0.5f*dy);
            cview_gl.rot_y += (int)(0.5f*dx);
        } else {
            int w = (cview_gl.window_w > 0) ? cview_gl.window_w : 1;
            int h = (cview_gl.window_h > 0) ? cview_gl.window_h : 1;
            cview_gl.d_x += (((GLfloat)dx)/((GLfloat)w))*cview_gl.rlen;
            cview_gl.d_y -= (((GLfloat)dy)/((GLfloat)h))*cview_gl.rlen;
        }

        /* refresh display */
        glutPostRedisplay();
    }
}

/*-------------------------------------------------------------------------
 * GLUT callback for window reshape
 */

static void cview_gl_reshape(int w, int h)
{
    glViewport(0,0,(GLsizei)w,(GLsizei)h);
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    if (w >= h) {
        GLdouble b = cview_gl.frustum_base;
        GLdouble r = (h <= 0) ? 1.0 : ((GLdouble)w)/((GLdouble)h);
        glFrustum(-r*b,r*b,-b,b,
            cview_gl.frustum_near,cview_gl.frustum_far);
    } else {
        GLdouble b = cview_gl.frustum_base;
        GLdouble r = (w <= 0) ? 1.0 : ((GLdouble)h)/((GLdouble)w);
        glFrustum(-b,b,-r*b,r*b,
            cview_gl.frustum_near,cview_gl.frustum_far);
    }
    glMatrixMode(GL_MODELVIEW);

    /* keep track of current window dimensions */
    cview_gl.window_w = w;
    cview_gl.window_h = h;
}

/*-------------------------------------------------------------------------
 * OpenGL setup and run forever
 *
 * pass all command line arguments to this routine,
 * for use by GLUT.
 *
 * cvdata_ptr cannot be NULL, and must already have data points added to it.
 * but cvdata_ptr->c3_ptr can be NULL.
 *
 * cvda_ptr can only be NULL if ncvda is zero.
 *
 * cvline_ptr can only be NULL if ncvline is zero.
 *
 * this routine never returns to caller,
 * exits program by calling exit().
 */

void cview_gl_main_loop(int argc, char *argv[], cview_data_t *cvdata_ptr,
                        uint32_t ncvda, const cview_draw_arrays_t *cvda_ptr,
                        uint32_t ncvline, const cview_line_t *cvline_ptr)
{
    /* sanity check vertex data */
    assert(cvdata_ptr != NULL);
    assert(cvdata_ptr->n > 0U);
    assert(cvdata_ptr->nmax >= cvdata_ptr->n);
    assert(cvdata_ptr->v3_ptr != NULL);
    assert(cvda_ptr != NULL || ncvda == 0U);
    assert(cvline_ptr != NULL || ncvline == 0U);

    /* initialize graphics/window state */
    cview_gl_init(cvdata_ptr,ncvda,cvda_ptr,ncvline,cvline_ptr);

    /* GLUT initialization */
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(cview_gl.window_w,cview_gl.window_h);
    glutInitWindowPosition(CVIEW_CFG_WIN_X,CVIEW_CFG_WIN_Y);
    cview_gl.window = glutCreateWindow("cview");

    /* setup display data */
    glEnable(GL_DEPTH_TEST);
    glClearColor(CVIEW_CFG_CLEAR_COLOR_R,
                 CVIEW_CFG_CLEAR_COLOR_G,
                 CVIEW_CFG_CLEAR_COLOR_B,
                 CVIEW_CFG_CLEAR_COLOR_A);
    glVertexPointer(3,GL_FLOAT,0,cview_gl.cvdata.v3_ptr);
    glColorPointer(3,GL_FLOAT,0,cview_gl.cvdata.c3_ptr);

    /* register callbacks with GLUT */
    glutDisplayFunc(cview_gl_display);
    glutReshapeFunc(cview_gl_reshape);
    glutSpecialFunc(cview_gl_special);
    glutKeyboardFunc(cview_gl_keyboard);
    glutMouseFunc(cview_gl_mouse);
    glutMotionFunc(cview_gl_motion);

    /* print keyboard mapping */
    cview_gl_print_keymap(NULL);

    /* actually display the window */
    glutMainLoop();
}

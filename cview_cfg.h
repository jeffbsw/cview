/*
 * cview_cfg.h - various constants for default and initial values
 *
 * Jeffrey Biesiadecki, 10/5/2019
 */

#ifndef CVIEW_CFG_H_
#define CVIEW_CFG_H_

/* initial window size and position on screen, in pixels */
#define CVIEW_CFG_WIN_W (700)
#define CVIEW_CFG_WIN_H (700)
#define CVIEW_CFG_WIN_X (100)
#define CVIEW_CFG_WIN_Y (0)

/* minimum length along any axis */
#define CVIEW_CFG_RMIN (1.0f)

/* radial length multiplier for camera position along Z */
#define CVIEW_CFG_ZVFACTOR (1.5f)

/* view frustum multiplier for frustum base */
#define CVIEW_CFG_FRUSTUM_BASE_FACTOR (0.05f)

/* view frustum multiplier for near clipping plane*/
#define CVIEW_CFG_FRUSTUM_NEAR_FACTOR (0.1f)

/* view frustum multiplier for far clipping plane*/
#define CVIEW_CFG_FRUSTUM_FAR_FACTOR (3.0f)

/* point size defaults and bounds, tracked as 16-bit unsigned integer */
#define CVIEW_CFG_POINT_SIZE_NFEW    (999U)
#define CVIEW_CFG_POINT_SIZE_DEFAULT (5U) /* def point size if few vertices */
#define CVIEW_CFG_POINT_SIZE_DELTA   (1U)
#define CVIEW_CFG_POINT_SIZE_MIN     (1U)
#define CVIEW_CFG_POINT_SIZE_MAX     (20U)

/* line width default, tracked as a 16-bit unsigned integer */
#define CVIEW_CFG_LINE_WIDTH_DEFAULT (2U)
#define CVIEW_CFG_LINE_WIDTH_DELTA   (1U)
#define CVIEW_CFG_LINE_WIDTH_MIN     (1U)
#define CVIEW_CFG_LINE_WIDTH_MAX     (7U)

/* plane half-size along diagonal */
#define CVIEW_CFG_PLANE_DIAG_PCT (25U)

/* plane color percent of normal line segmentcolor */
#define CVIEW_CFG_PLANE_COLOR_PCT (50U)

/* axis size defaults and bounds, tracked as 16-bit unsigned integer */
#define CVIEW_CFG_AXIS_PCT_DEFAULT (35U)
#define CVIEW_CFG_AXIS_PCT_DELTA   (10U)
#define CVIEW_CFG_AXIS_PCT_MIN     (5U)
#define CVIEW_CFG_AXIS_PCT_MAX     (85U)

/* default point color */
#define CVIEW_CFG_POINTS_COLOR_R (1.0f)
#define CVIEW_CFG_POINTS_COLOR_G (1.0f)
#define CVIEW_CFG_POINTS_COLOR_B (1.0f)

/* default center of rotation color */
#define CVIEW_CFG_ROT_CENTER_COLOR_R (1.0f)
#define CVIEW_CFG_ROT_CENTER_COLOR_G (1.0f)
#define CVIEW_CFG_ROT_CENTER_COLOR_B (1.0f)

/* default center of rotation point size offset */
#define CVIEW_CFG_ROT_CENTER_POINT_SIZE_OFFSET (3U)

/* default background color */
#define CVIEW_CFG_CLEAR_COLOR_R (0.0f)
#define CVIEW_CFG_CLEAR_COLOR_G (0.0f)
#define CVIEW_CFG_CLEAR_COLOR_B (0.0f)
#define CVIEW_CFG_CLEAR_COLOR_A (1.0f)

/* color map clipping fractions, from 0.0 to 1.0 */
#define CVIEW_CFG_ZMIN_FRACTION (0.0f)
#define CVIEW_CFG_ZMID_FRACTION (0.5f)
#define CVIEW_CFG_ZMAX_FRACTION (1.0f)

/* translation Z limits */
#define CVIEW_CFG_ZNEAR_FRACTION (1.5f) /* fraction of near clipping, gt 1 */
#define CVIEW_CFG_ZFAR_FRACTION (0.7f)  /* fraction of far clipping, lt 1 */

/* translation percentage per key press */
#define CVIEW_CFG_TRANSLATE_PCT_KEY (10U)
#define CVIEW_CFG_ROTATE_DEG_KEY    (10U)

#endif

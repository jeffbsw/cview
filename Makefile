#
# Makefile for compiling cloud-view,
# a simple point cloud viewer program
#
# can specify maximum number of vertices as 1000 (for example) by adding:
#     -DCVIEW_NMAX=1000
# to DEFINES variable below
#
# Jeffrey Biesiadecki, 9/21/2019
#

#--------------------------------------------------------------------------
# compilation tools and flags
#--------------------------------------------------------------------------

CC = gcc -std=c99
CFLAGS = -W -Wall -Werror -O
AR = ar
ARFLAGS = cr

PROG = cview
LIB = lib$(PROG)

HDR = cview.h cview_cfg.h cview_file.h kv_parse.h
LIBSRC = cview_data.c cview_file.c cview_gl.c kv_parse.c
PROGSRC = cview_main.c

DEFINES =
LIBS = -lglut -lGLU -lGL -lm

#--------------------------------------------------------------------------
# compilation rules, keeping it mindlessly simple
#--------------------------------------------------------------------------

LIBOBJ = $(patsubst %.c,%.o,$(LIBSRC))

# compilation rules
.PHONY: all
all: $(LIB).a $(PROG)

$(LIB).a: $(LIBSRC) $(HDR)
	$(CC) $(CFLAGS) $(DEFINES) -c $(LIBSRC)
	$(AR) $(ARFLAGS) $@ $(LIBOBJ)

$(PROG): $(LIB).a $(PROGSRC) $(HDR)
	$(CC) $(CFLAGS) $(DEFINES) -o $@ $(PROGSRC) $(LIB).a $(LIBS)

.PHONY: run
run: $(PROG)
	./$(PROG) --example \
                  --line 0 6.281 12 0 6.281 8 1 0 1 \
                  --normal 12.566 18.85 9 0 0 -1 1 0 1 1


.PHONY: clean
clean:
	rm -f *.o $(LIB).a $(PROG)

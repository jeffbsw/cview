/*
 * kv_parse.h - header file for keyworf/value parser objects
 *
 * Jeffrey Biesiadecki, 12/18/2016
 */

#ifndef KV_PARSE_H_
#define KV_PARSE_H_

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>

/*-------------------------------------------------------------------------
 * define types
 */

struct kv_parse_struct;

typedef int (*kv_parse_fn_t)(struct kv_parse_struct *kvp_ptr,
    int argc, char *argv[], void *client_data);

/* parser state */
typedef struct kv_parse_struct {
    kv_parse_fn_t fn;  /* function to call for each line of text */
    void *client_data; /* client data to pass to callback function */
    uint32_t lineno;   /* current parser line number */
    uint32_t nerror;   /* number of error messages reported */
} kv_parse_t;

/*-------------------------------------------------------------------------
 * declare functions
 */

/* allocate/deallocate parser object */
kv_parse_t *kv_parse_new(kv_parse_fn_t fn, void *client_data);
void kv_parse_delete(kv_parse_t *kvp_ptr);

/* initialize/clear contents of an already allocated object */
int kv_parse_init(kv_parse_t *kvp_ptr, kv_parse_fn_t fn, void *client_data);
void kv_parse_clear(kv_parse_t *kvp_ptr);

/* parse a file by name or already-open file pointer */
int kv_parse_file(kv_parse_t *kvp_ptr, const char *filename);
int kv_parse_fp(kv_parse_t *kvp_ptr, FILE *fp, uint32_t lineno);

/* convenience routines to simplify user callback */
void kv_parse_error(kv_parse_t *kvp_ptr, const char *errfmt, ...);
int kv_parse_int32(kv_parse_t *kvp_ptr, const char *s,
    int32_t *valptr, int32_t valmin, int32_t valmax);
int kv_parse_uint32(kv_parse_t *kvp_ptr, const char *s,
    uint32_t *valptr, uint32_t valmin, uint32_t valmax);
int kv_parse_double(kv_parse_t *kvp_ptr, const char *s,
    double *valptr, double valmin, double valmax);
int kv_parse_basename(char *dst, const char *src, size_t dst_nbytes);

#endif

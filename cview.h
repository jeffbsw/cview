/*
 * cview.h - header file for simple point cloud viewer
 *
 * Jeffrey Biesiadecki, 9/21/2019
 */

#ifndef CVIEW_H_
#define CVIEW_H_

#include <stdint.h>

#include <GL/gl.h>

#include "cview_cfg.h"

/*-------------------------------------------------------------------------
 * define public constants and types
 */

/* all vertices to render */
/* glVertexPointer will be set to v3_ptr, */
/* and glColorPointer will be set to c3_ptr, */
typedef struct {
    uint32_t nmax;   /* maximum number of vertices */
    uint32_t n;      /* number of vertices found */
    GLfloat *v3_ptr; /* vertices, MUST be allocated to at least 3*nmax elts! */
    GLfloat *c3_ptr; /* colors, MUST be allocated to at least 3*nmax elts! */
} cview_data_t;

/* max number of polygon edges */
/* must be representable by the "polygon" field of cview_draw_arrays_t */
/* and must be reasonable to allocate an array of this size on the stack */
enum { CVIEW_MAX_POLYGON = 255 }; /* cview_draw_arrays_t:polygon is uint8_t */

/* define a subset of vertices and how to render them with glDrawArrays */
/* a set contains "count" vertices in groups of "polygon" vertices */
/* if "count" not divisible by "polygon", then last polygon is not rendered */
typedef struct {
    uint32_t mode;   /* glDrawArrays mode to use for this set */
    uint32_t first;  /* index of first vertex in this set */
    uint32_t count;  /* num vertices in this set */
    uint8_t polygon; /* num vertices per fan/closed-line, or 0U */
    uint8_t prism; /* whether or not should connect polygon doubles in prism */
    uint8_t use_assigned_colors; /* whether or not colors already assigned */
    uint8_t use_solid_color;  /* whether or not all vertices are same color */
    GLfloat solid_color_r;    /* if solid color, red component for all */
    GLfloat solid_color_g;    /* if solid color, green component for all */
    GLfloat solid_color_b;    /* if solid color, blue component for all */
    GLfloat solid_color_a;    /* if solid color, alpha component for all */
} cview_draw_arrays_t;

/* line rendering style */
/* enum must start at 0, increment by 1, and keep NUM define in sync */
typedef enum {
    CVIEW_LINE_STYLE_NONE = 0, /* do not render line segment/plane at all */
    CVIEW_LINE_STYLE_NO_PLANE, /* simple line between endpoints, no plane */
    CVIEW_LINE_STYLE_PLANE     /* plane at start point, default scale */
    /* keep in sync with CVIEW_NUM_LINE_STYLE below! */
} cview_line_style_t;

#define CVIEW_NUM_LINE_STYLE (CVIEW_LINE_STYLE_PLANE+1U) /* see above! */

/* solid-color line segment to render */
typedef struct {
    GLfloat x0; /* X coordinate of first endpoint */
    GLfloat y0; /* Y coordinate of first endpoint */
    GLfloat z0; /* Z coordinate of first endpoint */
    GLfloat x1; /* X coordinate of second endpoint */
    GLfloat y1; /* Y coordinate of second endpoint */
    GLfloat z1; /* Z coordinate of second endpoint */
    GLfloat r;  /* line segment color, red, 0.0..1.0 */
    GLfloat g;  /* line segment color, green, 0.0..1.0 */
    GLfloat b;  /* line segment color, blue, 0.0..1.0 */
    uint8_t plane; /* whether or not to draw plane at start */
} cview_line_t;

/* statistics about vertex data */
typedef struct {
    uint32_t n;         /* number of samples */
    GLfloat vmin;       /* minimum sample value */
    GLfloat vmax;       /* maximum sample value */
    GLfloat vmean;      /* mean sample value */
    GLfloat vstddev;    /* standard deviation from mean */
    GLfloat vmaxabsdev; /* maximum absolute deviation from mean */
} cview_statistics_t;

/* function of x and y, for height map generation */
typedef GLfloat (*cview_data_generate_fn_t)(GLfloat vx, GLfloat vy,
                                            void *fn_client_data);

/*-------------------------------------------------------------------------
 * declare public functions
 */

/* cview_data.c */
void cview_data_init(cview_data_t *cvdata_ptr,
                     uint32_t nmax, GLfloat *v3_ptr, GLfloat *c3_ptr);
int cview_data_add_xyz(cview_data_t *cvdata_ptr,
                       GLfloat vx, GLfloat vy, GLfloat vz);
void cview_data_statistics(const cview_data_t *cvdata_ptr,
                           cview_statistics_t *xstats_ptr,
                           cview_statistics_t *ystats_ptr,
                           cview_statistics_t *zstats_ptr);
GLfloat cview_data_interp(GLfloat x,
                          GLfloat x0, GLfloat x1,
                          GLfloat y0, GLfloat y1);
void cview_data_color(cview_data_t *cvdata_ptr,
                      uint32_t ncvda, const cview_draw_arrays_t *cvda_ptr);
int cview_data_generate(cview_data_t *cvdata_ptr,
                        uint32_t nx, uint32_t ny,
                        GLfloat xmin, GLfloat xmax, GLfloat ymin, GLfloat ymax,
                        cview_data_generate_fn_t fn, void *fn_client_data);

/* cview_gl.c */
void cview_gl_main_loop(int argc, char *argv[], cview_data_t *cvdata_ptr,
                        uint32_t ncvda, const cview_draw_arrays_t *cvda_ptr,
                        uint32_t ncvline, const cview_line_t *cvline_ptr);

#endif

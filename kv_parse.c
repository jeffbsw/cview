/*
 * kv_parse.c - keyword/value parser object
 *
 * Jeffrey Biesiadecki, 12/18/2016
 */

/* define these tokens for strsep */
#define _BSD_SOURCE
#define _DEFAULT_SOURCE
 
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

#include "kv_parse.h"

/*-------------------------------------------------------------------------
 * define private constants and macros
 */

/* maximum number of characters in a keyword-value line */
#define KV_PARSE_LINE_MAX_NCHAR (512U)

/* maximum number of tokens in a keyword-value line */
#define KV_PARSE_LINE_MAX_NTOKEN (1U+KV_PARSE_LINE_MAX_NCHAR/2U)

/* maximum number of characters in error message when parsing */
/* keyword-value pairs */
#define KV_PARSE_ERROR_MAX_NCHAR (512U)

/*-------------------------------------------------------------------------
 * declare private functions
 */

static void kv_chomp(char *bptr);
static int32_t kv_tokenize(char *buf, uint32_t nargv, char *argv[]);
static int kv_parse_fgets(kv_parse_t *kvp_ptr, FILE *fp, char *buf, size_t nbuf);

/*-------------------------------------------------------------------------
 * allocate a parser object
 *
 * returns NULL on error
 */

kv_parse_t *kv_parse_new(kv_parse_fn_t fn, void *client_data)
{
    /* allocate and initialize object */
    kv_parse_t *kvp_ptr = malloc(sizeof(kv_parse_t));
    if (kvp_ptr != NULL) {
        /* initialize object */
        if (kv_parse_init(kvp_ptr,fn,client_data) != 0) {
            kv_parse_clear(kvp_ptr);
            free(kvp_ptr);
            kvp_ptr = NULL;
        }
    }

    /* all done */
    return kvp_ptr;
}

/*-------------------------------------------------------------------------
 * free a parser object
 */

void kv_parse_delete(kv_parse_t *kvp_ptr)
{
    if (kvp_ptr != NULL) {
        kv_parse_clear(kvp_ptr);
        free(kvp_ptr);
    }
}

/*-------------------------------------------------------------------------
 * initialize an already allocated parser object
 *
 * returns 0 if successful, -1 otherwise
 */

int kv_parse_init(kv_parse_t *kvp_ptr, kv_parse_fn_t fn, void *client_data)
{
    int status = -1;

    if (kvp_ptr != NULL) {
        status = 0;

        /* make sure all fields are initialized to something */
        (void)memset(kvp_ptr,'\0',sizeof(*kvp_ptr));

        /* initialize fields */
        kvp_ptr->fn = fn;
        kvp_ptr->client_data = client_data;
        kvp_ptr->lineno = 0U;
        kvp_ptr->nerror = 0U;
    }

    return status;
}

/*-------------------------------------------------------------------------
 * finish with an already allocated parser object,
 * closing any files opened by and freeing any memory allocated by
 * the parser object, but does not deallocate the parser itself
 */

void kv_parse_clear(kv_parse_t *kvp_ptr)
{
    /* current implementation has no files to close or memory to free */
    (void)kv_parse_init(kvp_ptr,0,NULL);
}

/*-------------------------------------------------------------------------
 * report an error condition encountered while parsing
 */

void kv_parse_error(kv_parse_t *kvp_ptr, const char *errfmt, ...)
{
    if (kvp_ptr != NULL) {
        uint32_t lineno = kvp_ptr->lineno;
        char errmsg[KV_PARSE_ERROR_MAX_NCHAR];

        if (errfmt != NULL) {
            size_t noffset = 0U;

            /* start with line number, if supplied */
            if (lineno > 0U) {
                int n = snprintf(errmsg,sizeof(errmsg),
                    "line %u, ",(unsigned int)lineno);
                if (n > 0) {
                    noffset = (size_t)n;
                    if (noffset > sizeof(errmsg)) {
                        noffset = sizeof(errmsg);
                    }
                }
            }

            /* append user-supplied error message */
            va_list ap;
            va_start(ap,errfmt);
            (void)vsnprintf(errmsg+noffset,sizeof(errmsg)-noffset,errfmt,ap);
            va_end(ap);
        } else if (lineno > 0U) {
            /* no user-supplied error message, just report line number */
            (void)snprintf(errmsg,sizeof(errmsg),"line %u",(unsigned int)lineno);
        } else {
            /* no user-supplied error message and no line number - good luck! */
            (void)snprintf(errmsg,sizeof(errmsg),"parsing");
        }

        /* report error message */
        (void)fprintf(stderr,"error: %s\n",errmsg);
        kvp_ptr->nerror += 1U;
    }
}

/*-------------------------------------------------------------------------
 * read keywords/values from specified file
 *
 * return 0 if successful, -1 on any error
 */

int kv_parse_file(kv_parse_t *kvp_ptr, const char *filename)
{
    int status = -1;

    if (kvp_ptr != NULL) {
        kvp_ptr->lineno = 0U;
        if (filename == NULL) {
            kv_parse_error(kvp_ptr,"kv_parse_file provided NULL filename");
        } else {
            FILE *fp = fopen(filename,"r");
            if (fp == NULL) {
                kv_parse_error(kvp_ptr,
                    "kv_parse_file could not open '%s' for reading",filename);
            } else {
                status = kv_parse_fp(kvp_ptr,fp,0U);
                fclose(fp);
            }
        }
    }

    return status;
}

/*-------------------------------------------------------------------------
 * read keywords/values from specified file pointer
 *
 * pass the number of lines already read from the file pointer in lineno,
 * which normally will be zero
 *
 * return 0 if successful, -1 on any error
 */

int kv_parse_fp(kv_parse_t *kvp_ptr, FILE *fp, uint32_t lineno)
{
    int status = -1;

    if (kvp_ptr != NULL) {
        kvp_ptr->lineno = lineno;
        if (fp == NULL) {
            kv_parse_error(kvp_ptr,"kv_parse_fp provided NULL file pointer");
        } else if (kvp_ptr->fn == 0) {
            kv_parse_error(kvp_ptr,"kv_parse_fp and no application callback");
        } else {
            for ( ; ; ) {
                char buf[KV_PARSE_LINE_MAX_NCHAR];

                /* read another non-blank/non-comment line, if any more */
                int fstatus = kv_parse_fgets(kvp_ptr,fp,buf,sizeof(buf));
                if (fstatus <= 0) {
                    if (fstatus == 0) {
                        status = 0; /* reached end of file without an error */
                    }
                    break;
                }

                /* tokenize this line */
                char *argv[KV_PARSE_LINE_MAX_NTOKEN];
                int argc = kv_tokenize(buf,sizeof(argv)/sizeof(argv[0]),argv);
                if (argc < 0) {
                    kv_parse_error(kvp_ptr,
                        "too many tokens, can be at most %u",
                        sizeof(argv)/sizeof(argv[0]));
                    break;
                }

                /* call application callback */
                uint32_t nerror = kvp_ptr->nerror;
                int ustatus =
                    kvp_ptr->fn(kvp_ptr,argc,argv,kvp_ptr->client_data);
                if (ustatus != 0) {
                    if (kvp_ptr->nerror == nerror) {
                        /* user returned error status but no error message */
                        kv_parse_error(kvp_ptr,
                            "application callback returned status %d",ustatus);
                    }
                    break;
                }
            }
        }
    }

    return status;
}

/*-------------------------------------------------------------------------
 * parse one signed integer
 *
 * report an error if string does not hold only a valid integer,
 * or if it is out-of-range
 *
 * does not check range if valmin >= valmax
 *
 * s - the string holding only the characters for a single integer
 * valptr - the parsed integer (or 0 if the string is invalid)
 * valmin - minimum valid value
 * valmax - minimum valid value
 *
 * returns 0 if successful, -1 on any error
 */

int kv_parse_int32(kv_parse_t *kvp_ptr, const char *s,
    int32_t *valptr, int32_t valmin, int32_t valmax)
{
    int status = -1;
    int32_t val = 0;

    /* sanity check arguments from caller */
    assert(s != NULL);

    /* parse value */
    if (kvp_ptr != NULL) {
        char *last_char = '\0';
        val = (int32_t)strtol(s,&last_char,0);

        if (last_char != NULL && *last_char != '\0') {
            kv_parse_error(kvp_ptr,"expected integer, found '%s'",s);
        } else if (valmin < valmax && (val < valmin || val > valmax)) {
            kv_parse_error(kvp_ptr,
                "integer '%s' is out of range %d %d",
                s,(int)valmin,(int)valmax);
        } else {
            status = 0;
        }
    }

    /* save something */
    if (valptr != NULL) {
        *valptr = (status == 0) ? val : 0;
    }
    return status;
}

/*-------------------------------------------------------------------------
 * parse one unsigned integer
 *
 * report an error if string does not hold only a valid unsigned integer,
 * or if it is out-of-range
 *
 * does not check range if valmin >= valmax
 *
 * s - the string holding only the characters for a single unsigned integer
 * valptr - the parsed unsigned integer (or 0 if the string is invalid)
 * valmin - minimum valid value
 * valmax - minimum valid value
 *
 * returns 0 if successful, -1 on any error
 */

int kv_parse_uint32(kv_parse_t *kvp_ptr, const char *s,
    uint32_t *valptr, uint32_t valmin, uint32_t valmax)
{
    int status = -1;
    uint32_t val = 0U;

    /* sanity check arguments from caller */
    assert(s != NULL);

    /* parse value */
    if (kvp_ptr != NULL) {
        char *last_char = '\0';
        val = (uint32_t)strtoul(s,&last_char,0);

        if (last_char != NULL && *last_char != '\0') {
            kv_parse_error(kvp_ptr,"expected unsigned integer, found '%s'",s);
        } else if (valmin < valmax && (val < valmin || val > valmax)) {
            kv_parse_error(kvp_ptr,
                "unsigned integer '%s' is out of range %u=0x%08X %u=0x%08X",
                s,(unsigned int)valmin,(unsigned int)valmin,
                (unsigned int)valmax,(unsigned int)valmax);
        } else {
            status = 0;
        }
    }

    /* save something */
    if (valptr != NULL) {
        *valptr = (status == 0) ? val : 0U;
    }
    return status;
}

/*-------------------------------------------------------------------------
 * parse one real number
 *
 * report an error if string does not hold only a valid number,
 * or if it is out-of-range
 *
 * does not check range if valmin >= valmax
 *
 * s - the string holding only the characters for a single real number
 * valptr - the parsed number (or 0 if the string is invalid)
 * valmin - minimum valid value
 * valmax - minimum valid value
 *
 * returns 0 if successful, -1 on any error
 */

int kv_parse_double(kv_parse_t *kvp_ptr, const char *s,
    double *valptr, double valmin, double valmax)
{
    int status = -1;
    double val = 0.0;

    /* sanity check arguments from caller */
    assert(s != NULL);

    /* parse value */
    if (kvp_ptr != NULL) {
        char *last_char = '\0';
        val = strtod(s,&last_char);

        if (last_char != NULL && *last_char != '\0') {
            kv_parse_error(kvp_ptr,"expected real number, found '%s'",s);
        } else if (valmin < valmax && (val < valmin || val > valmax)) {
            kv_parse_error(kvp_ptr,
                "double '%s' is out of range %g %g",
                s,valmin,valmax);
        } else {
            status = 0;
        }
    }

    /* save something */
    if (valptr != NULL) {
        *valptr = (status == 0) ? val : 0.0;
    }
    return status;
}

/*-------------------------------------------------------------------------
 * copy base filename, the characters between last slash and last period
 *
 * returns 0 if result fits in dst_nbytes, -1 otherwise
 */

int kv_parse_basename(char *dst, const char *src, size_t dst_nbytes)
{
    int status = -1;

    if (dst != NULL && dst_nbytes > 0U) {
        status = 0;

        if (src == NULL) {
            *dst = '\0';
        } else {
            const char *last_slash = NULL;
            const char *last_period = NULL;
            const char *p;

            /* find last slash and period, if any, and '\0' */
            for (p = src; *p != '\0'; p++) {
                if (*p == '/') {
                    last_slash = p;
                    last_period = NULL; /* only want period after last slash */
                } else if (*p == '.') {
                    last_period = p;
                } else {
                    /* nothing */
                }
            }

            /* copy characters, saving room for terminating '\0' */
            const char *e = (last_period == NULL) ? p : last_period;
            p = (last_slash == NULL) ? src : (last_slash+1);
            while (p < e) {
                if (dst_nbytes <= 1U) {
                    status = -1;
                    break;
                }

                *dst++ = *p++;
                dst_nbytes -= 1U;
            }

            /* terminate destination string */
            *dst = '\0';
        }
    }

    return status;
}

/*-------------------------------------------------------------------------
 * chomp whitespace in a '\0'-terminated string
 *
 * leading and trailing whitespace is removed, and repeated whitespace
 * collapsed into a single space
 *
 * comments start with '#' and extend to end of line, and are removed
 */

static void kv_chomp(char *bptr)
{
    if (bptr != NULL) {
        int was_space = 0;
        int found_nonspace = 0;
        char *sptr = bptr;
        for ( ; ; ) {
            /* look for string terminator, or comment start */
            if (*bptr == '\0' || *bptr == '#') {
                *sptr = '\0';
                break;
            } else if (isspace(*bptr)) {
                was_space = found_nonspace;
                bptr++;
            } else {
                found_nonspace = 1;
                if (was_space) {
                    was_space = 0;
                    *sptr++ = ' ';
                }
                *sptr++ = *bptr++;
            }
        }
    }
}

/*-------------------------------------------------------------------------
 * tokenize a space-separated and '\0'-terminated character string
 *
 * normally buf should be run through kv_chomp first, to collapse
 * multiple white-space characters into a single space
 *
 * modifies the string passed in, replacing each space between words with a
 * terminating '\0'
 *
 * argv must have been allocated by caller to at least nargv entries
 *
 * only the entries in argv up to the number of tokens actually present
 * have their pointers assigned, plus one NULL argv entry only if there are
 * fewer than nargv tokens in buf.  so there is no guarantee of a
 * trailing NULL argv entry.
 *
 * returns -1 if there were more tokens in buf than nargv, otherwise,
 * the number of tokens found is returned
 */

static int32_t kv_tokenize(char *buf, uint32_t nargv, char *argv[])
{
    int32_t ntoken = -1;

    /* sanity check arguments from caller */
    assert(argv != NULL || nargv == 0U);
    assert(((int32_t)nargv) >= 0);

    uint32_t i;

    /* remember a pointer to each token in buf */
    for (i = 0U; i < nargv; i++) {
        argv[i] = strsep(&buf," ");
        if (argv[i] == NULL) {
            break;
        } 
    }

    /* only return success if all words were tokenized */
    if (buf == NULL) {
        ntoken = (int32_t)i;
    }

    return ntoken;
}

/*-------------------------------------------------------------------------
 * read lines from file until a non-blank/non-comment line is found
 *
 * copy it into buf which must be sized to hold at least nbuf characters,
 * and buf will always be '\0'-terminated
 *
 * returns 1 if another line found, 0 if not, -1 on any error
 */

static int kv_parse_fgets(kv_parse_t *kvp_ptr, FILE *fp, char *buf, size_t nbuf)
{
    int status = -1;

    /* check arguments from caller */
    if (kvp_ptr != NULL && fp != NULL && buf != NULL && nbuf > 0U) {
        int found = 0;

        /* read lines until a non-blank/non-comment line found, EOF, or error */
        while (fgets(buf,(int)nbuf,fp) != NULL) {
            /* increment line count */
            kvp_ptr->lineno += 1U;

            /* ensure fgets could read entire line without truncation */
            /* could allow this if comment character found by kv_chomp */
            size_t nlen = strlen(buf);
            if (nlen > 0U && buf[nlen-1U] != '\n') {
                kv_parse_error(kvp_ptr,
                    "line too long, can be at most %u characters",
                    (unsigned int)nbuf);
                break;
            }

            /* collapse extra spaces and remove comments */
            kv_chomp(buf);

            /* see if anything left after chomping */
            if (buf[0] != '\0') {
                found = 1;
                break;
            }
        }

        /* ensure return string is terminated if all remaining lines */
        /* were blank/comment */
        if (!found) {
            buf[0] = '\0';
            status = 0;
        } else {
            status = 1;
        }

        /* see if an error condition occurred */
        int fstatus = ferror(fp);
        if (fstatus != 0) {
            kv_parse_error(kvp_ptr,"ferror returned %d",fstatus);
            status = -1;
        }
    }

    return status;
}

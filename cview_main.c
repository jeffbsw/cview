/*
 * cview_main.c - main routine for simple point cloud viewer
 *
 * Jeffrey Biesiadecki, 9/21/2019
 */

#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glut.h>

#include "cview.h"
#include "cview_file.h"

/*-------------------------------------------------------------------------
 * define private constants, types, and global variables
 */

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327)
#endif

/* storage for vertices */
/* normally too large for stack */
#ifndef CVIEW_NMAX
#define CVIEW_NMAX (4000000U)
#endif
GLfloat cview_v3[3U*CVIEW_NMAX]; /* list of X, Y, Z coordinates */
GLfloat cview_c3[3U*CVIEW_NMAX]; /* list of R, G, B colors */

/* storage for vertex set definitions */
/* make this global to facilitate defining more than would want on stack */
#ifndef CVIEW_DRAW_ARRAYS_NMAX
#define CVIEW_DRAW_ARRAYS_NMAX (32U)
#endif
cview_draw_arrays_t cview_draw_arrays[CVIEW_DRAW_ARRAYS_NMAX];

/* storage for individual line segments and normal vectors */
/* make this global to facilitate defining more than would want on stack */
#ifndef CVIEW_LINE_NMAX
#define CVIEW_LINE_NMAX (32U)
#endif
cview_line_t cview_line[CVIEW_LINE_NMAX];

/* epsilon value for length of normal vector */
#define CVIEW_NORMAL_EPS (0.0001f)

/*-------------------------------------------------------------------------
 * print usage message and exit with status 2
 */

static void usage(const char *errfmt, ...)
{
    if (errfmt != NULL) {
        va_list ap;
        va_start(ap,errfmt);
        fprintf(stderr,"error: ");
        vfprintf(stderr,errfmt,ap);
        fprintf(stderr,"\n");
        va_end(ap);
    }

    fprintf(stderr,
        "usage: cview <options> <cvf0.txt> ... [-- <GLUT options>]\n"
        "where <options> may be:\n"
        "  -h|--help           get this helpful message\n"
        "  -a|--auto-color     "
           "color subsequent vertex sets based on Z (default)\n"
        "  -c|--color r g b    constant color (0.0-1.0)\n"
        "                      for all vertices in subsequent vertex sets\n"
        "  -e|--example        generate an example point cloud\n"
        "  -f|--flat           single flat polygons "
                               "for subsequent vertex sets (default)\n"
        "  -l|--line x0 y0 z0 x1 y1 z1 r g b      "
                               "add line segment and color\n"
        "  -p|--polygon p      polygon type to interpret/render "
                               "subsequent vertex sets:\n"
        "                      "
        "pt|line|tri|quad|hex|oct|<n> (default is pt)\n"
        "  -n|--normal x0 y0 z0 xn yn zn len r g b  "
                                 "add normal+plane and color\n"
        "  -P|--prism          double up polygons into prisms "
                               "for subsequent vertex sets\n"
        "  -s|--solid          "
            "fill polygons for subsequent vertex sets (default)\n"
        "  -w|--wire           "
            "wire frame polygons for subsequent vertex sets\n"
        "  --                  remaining arguments are for glutInit()\n"
        "<cvfN.txt>            file defining a set of vertices,\n"
        "                      ASCII X Y Z coordinates and space-separated\n"
        "\n"
        "This program is a simple point cloud viewer,\n"
        "which reads sets of vertices to render from text files.\n"
        "\n"
        "Vertex data files are ASCII text, with one X Y Z vertex per line,\n"
        "coordinates separated with spaces and not commas.\n"
        "Comments start with #, and blank lines are skipped.\n"
        "\n"
        "For the line mode, the subsequent vertex sets\n"
        "are interpreted as being a list of independent line segments.\n"
        "If the number of vertices in the set is not divisible by two,\n"
        "then the last (partial) line segment is not rendered.\n"
        "\n"
        "For tri polygons, the subsequent vertex sets are interpreted\n"
        "as being a list of independent triangles.\n"
        "If the number of vertices in the set is not divisible by three,\n"
        "then the last (partial) triangle is not rendered.\n"
        "\n"
        "For quad polygons, the subsequent vertex sets are interpreted\n"
        "as being a list of independent quadrilaterals.\n"
        "If the number of vertices in the set is not divisible by four,\n"
        "then the last (partial) quadrilateral is not rendered.\n"
        "\n"
        "You can guess how hex and oct polygon modes work.\n"
        "\n"
        "These non-pt and non-line polygon modes (tri, quad, etc.)\n"
        "can also be rendered as prisms, where the polygons are rendered\n"
        "in top/bottom pairs (with sides).  The default is flat rendering.\n"
        "\n"
        "This program is compiled for at most %u vertices,\n"
        "split into at most %u sets of vertices,\n"
        "and %u individually-rendered line segments or normal vectors.\n",
        CVIEW_NMAX,CVIEW_DRAW_ARRAYS_NMAX,CVIEW_LINE_NMAX);

    exit(2);
}

/*-------------------------------------------------------------------------
 * parse a double, or generate a usage error (which exits program)
 *
 * also range checks if dmin is strictly less than dmax
 */

static double parse_double_arg(const char *s, double dmin, double dmax)
{
    assert(s != NULL);

    char *last_char = '\0';
    double v = strtod(s,&last_char);
    if (last_char == NULL || *last_char != '\0') {
        usage("expected real number, found '%s'",s);
    } else if (dmin < dmax && (v < dmin || v > dmax)) {
        usage("should be between %g and %g, found %g",dmin,dmax,v);
    }
    return v;
}

/*-------------------------------------------------------------------------
 * parse an unsigned long, or generate a usage error (which exits program)
 *
 * also range checks if dmin is strictly less than dmax
 */

static unsigned long int parse_ulong_arg(const char *s,
    unsigned long int dmin, unsigned long int dmax)
{
    assert(s != NULL);

    char *last_char = '\0';
    unsigned long int v = strtoul(s,&last_char,10);
    if (last_char == NULL || *last_char != '\0') {
        usage("expected unsigned integer, found '%s'",s);
    } else if (dmin < dmax && (v < dmin || v > dmax)) {
        usage("should be between %u and %u, found %u",dmin,dmax,v);
    }
    return v;
}

/*-------------------------------------------------------------------------
 * callback function used to generate example point cloud
 */

GLfloat cview_gen_fn(GLfloat vx, GLfloat vy, void *fn_client_data)
{
    (void)fn_client_data; /* client data not needed for this callback */

    return 10.0f-cos(vx)*cos(vy);
}

/*-------------------------------------------------------------------------
 * generate a height map for an example point cloud
 *
 * returns 0 if all points added successfully
 */

int cview_gen(cview_data_t *cvdata_ptr)
{
    double xmin = -2.0*M_PI;
    double xmax =  6.0*M_PI;
    double ymin = -0.0*M_PI;
    double ymax =  8.0*M_PI;
    uint32_t nx = 701U; /* must be greater than 1 */
    uint32_t ny = 701U; /* must be greater than 1 */

    return cview_data_generate(cvdata_ptr,nx,ny,xmin,xmax,ymin,ymax,
                               cview_gen_fn,NULL);
}

/*-------------------------------------------------------------------------
 * main program for point cloud viewer
 */

int main(int argc, char *argv[])
{
    int status = 0;

    /* initialize vertex data */
    cview_data_t cvdata;
    cview_data_init(&cvdata,CVIEW_NMAX,cview_v3,cview_c3);

    /* initialize vertex set data */
    memset(cview_draw_arrays,'\0',sizeof(cview_draw_arrays));
    uint32_t ncvda = 0U;

    /* initialize line segment and normal vector data */
    memset(cview_line,'\0',sizeof(cview_line));
    uint32_t ncvline = 0U;

    /* modes and color for subsequent sets of vertices */
    uint32_t polygon = 1U;
    uint8_t wire_frame = 0U;
    uint8_t prism = 0U;
    uint8_t use_assigned_colors = 0U;
    uint8_t use_solid_color = 0U;
    GLfloat solid_color_r = CVIEW_CFG_POINTS_COLOR_R; /* defensive */
    GLfloat solid_color_g = CVIEW_CFG_POINTS_COLOR_G; /* defensive */
    GLfloat solid_color_b = CVIEW_CFG_POINTS_COLOR_B; /* defensive */

    /* parse command line and load vertex data files */
    /* note that arguments will be passed on to glutInit(), */
    /* so do not complain about any unrecognized arguments */
    int i;
    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i],"-h") == 0 || strcmp(argv[i],"--help") == 0) {
            usage(NULL);
        } else if (strcmp(argv[i],"-a") == 0 ||
            strcmp(argv[i],"--auto-color") == 0) {
            use_assigned_colors = 0U;
            use_solid_color = 0U;
            solid_color_r = CVIEW_CFG_POINTS_COLOR_R; /* defensive */
            solid_color_g = CVIEW_CFG_POINTS_COLOR_G; /* defensive */
            solid_color_b = CVIEW_CFG_POINTS_COLOR_B; /* defensive */
        } else if (strcmp(argv[i],"-c") == 0 ||
            strcmp(argv[i],"--color") == 0) {
            if (i+3 >= argc) {
                usage("must specify 3 arguments to define color");
            } else {
                use_assigned_colors = 0U;
                use_solid_color = 1U;
                solid_color_r = parse_double_arg(argv[++i],0.0,1.0);
                solid_color_g = parse_double_arg(argv[++i],0.0,1.0);
                solid_color_b = parse_double_arg(argv[++i],0.0,1.0);
            }
        } else if (strcmp(argv[i],"-e") == 0 ||
            strcmp(argv[i],"--example") == 0) {
            if (ncvda >= CVIEW_DRAW_ARRAYS_NMAX) {
                usage("too many sets of vertices defined, maximum is %u",
                      CVIEW_DRAW_ARRAYS_NMAX);
            } else {
                /* generate vertices from f(x,y) */
                uint32_t first = cvdata.n;
                status = cview_gen(&cvdata);
                if (status != 0) {
                    usage("error generated too many vertices");
                }
                if (cvdata.n <= first) {
                    usage("error no vertices were generated");
                }

                /* define a new set specifying these vertices */
                cview_draw_arrays_t *cvda_ptr = cview_draw_arrays+ncvda;
                cvda_ptr->mode = GL_POINTS;
                cvda_ptr->first = first;
                cvda_ptr->count = cvdata.n - first;
                cvda_ptr->polygon = 0U;
                cvda_ptr->prism = 0U;
                cvda_ptr->use_assigned_colors = use_assigned_colors;
                cvda_ptr->use_solid_color = use_solid_color;
                cvda_ptr->solid_color_r = solid_color_r;
                cvda_ptr->solid_color_g = solid_color_g;
                cvda_ptr->solid_color_b = solid_color_b;
                ncvda += 1U;
            }
        } else if (strcmp(argv[i],"-f") == 0 || strcmp(argv[i],"--flat") == 0) {
            prism = 0U;
        } else if (strcmp(argv[i],"-l") == 0 || strcmp(argv[i],"--line") == 0) {
            if (i+9 >= argc) {
                usage("must specify 9 arguments to define line segment");
            } else if (ncvline >= CVIEW_LINE_NMAX) {
                usage("too many lines defined, maximum is %u",CVIEW_LINE_NMAX);
            } else {
                /* parse segment endpoints and colors from command line args */
                GLfloat x0 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat y0 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat z0 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat x1 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat y1 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat z1 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat r = parse_double_arg(argv[++i],0.0,1.0);
                GLfloat g = parse_double_arg(argv[++i],0.0,1.0);
                GLfloat b = parse_double_arg(argv[++i],0.0,1.0);

                /* add line segment, flip to OpenGL coordinates */
                cview_line_t *cvline_ptr = cview_line+ncvline;
                cvline_ptr->x0 = y0;
                cvline_ptr->y0 = x0;
                cvline_ptr->z0 = -z0;
                cvline_ptr->x1 = y1;
                cvline_ptr->y1 = x1;
                cvline_ptr->z1 = -z1;
                cvline_ptr->r = r;
                cvline_ptr->g = g;
                cvline_ptr->b = b;
                cvline_ptr->plane = 0U;
                ncvline += 1U;
            }
        } else if (strcmp(argv[i],"-n") == 0 ||
                   strcmp(argv[i],"--normal") == 0) {
            if (i+10 >= argc) {
                usage("must specify 10 arguments to define normal");
            } else if (ncvline >= CVIEW_LINE_NMAX) {
                usage("too many lines defined, maximum is %u",CVIEW_LINE_NMAX);
            } else {
                /* parse segment endpoints and colors from command line args */
                GLfloat x0 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat y0 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat z0 = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat xn = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat yn = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat zn = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat len = parse_double_arg(argv[++i],0.0,0.0);
                GLfloat r = parse_double_arg(argv[++i],0.0,1.0);
                GLfloat g = parse_double_arg(argv[++i],0.0,1.0);
                GLfloat b = parse_double_arg(argv[++i],0.0,1.0);
                GLfloat nlen = sqrtf(xn*xn+yn*yn+zn*zn);
                GLfloat scale = (nlen > CVIEW_NORMAL_EPS) ? len/nlen : 0.0f;
                GLfloat x1 = x0 + scale*xn;
                GLfloat y1 = y0 + scale*yn;
                GLfloat z1 = z0 + scale*zn;

                /* add line segment, flip to OpenGL coordinates */
                cview_line_t *cvline_ptr = cview_line+ncvline;
                cvline_ptr->x0 = y0;
                cvline_ptr->y0 = x0;
                cvline_ptr->z0 = -z0;
                cvline_ptr->x1 = y1;
                cvline_ptr->y1 = x1;
                cvline_ptr->z1 = -z1;
                cvline_ptr->r = r;
                cvline_ptr->g = g;
                cvline_ptr->b = b;
                cvline_ptr->plane = 1U;
                ncvline += 1U;
            }
        } else if (strcmp(argv[i],"-p") == 0 ||
                   strcmp(argv[i],"--polygon") == 0) {
            if (i+1 >= argc) {
                usage("must specify a mode argument for -p/--polygon");
            } else {
                const char *mode_str = argv[++i];
                if (strcmp(mode_str,"pt") == 0) {
                    polygon = 1U;
                } else if (strcmp(mode_str,"line") == 0) {
                    polygon = 2U;
                } else if (strcmp(mode_str,"tri") == 0) {
                    polygon = 3U;
                } else if (strcmp(mode_str,"quad") == 0) {
                    polygon = 4U;
                } else if (strcmp(mode_str,"hex") == 0) {
                    polygon = 6U;
                } else if (strcmp(mode_str,"oct") == 0) {
                    polygon = 8U;
                } else {
                    polygon = (uint32_t)parse_ulong_arg(mode_str,
                                                        1U,CVIEW_MAX_POLYGON);
                }
            }
        } else if (strcmp(argv[i],"-P") == 0 ||
                   strcmp(argv[i],"--prism") == 0) {
            prism = 1U;
        } else if (strcmp(argv[i],"-s") == 0 ||
                   strcmp(argv[i],"--solid") == 0) {
            wire_frame = 0U;
        } else if (strcmp(argv[i],"-w") == 0 || strcmp(argv[i],"--wire") == 0) {
            wire_frame = 1U;
        } else if (strcmp(argv[i],"--") == 0) {
            break;
        } else if (argv[i][0] == '-') {
            usage("unknown option '%s' specified",argv[i]);
        } else {
            if (ncvda >= CVIEW_DRAW_ARRAYS_NMAX) {
                usage("too many sets of vertices defined, maximum is %u",
                      CVIEW_DRAW_ARRAYS_NMAX);
            } else {
                /* add vertices from specified vertex data file */
                uint32_t first = cvdata.n;
                status = cview_file_read(&cvdata,argv[i]);
                if (status != 0) {
                    usage("error reading vertex data file '%s'",argv[i]);
                }
                if (cvdata.n <= first) {
                    usage("error nothing in vertex data file '%s'",argv[i]);
                }

                /* define a new set specifying these vertices */
                cview_draw_arrays_t *cvda_ptr = cview_draw_arrays+ncvda;
                uint32_t mode = GL_POINTS;
                if (polygon > 1U) {
                    if (polygon == 2U) {
                        mode = GL_LINES;
                    } else if (wire_frame) {
                        mode = GL_LINE_LOOP;
                    } else {
                        mode = GL_TRIANGLE_FAN;
                    }
                }
                cvda_ptr->mode = mode;
                cvda_ptr->first = first;
                cvda_ptr->count = cvdata.n - first;
                cvda_ptr->polygon = polygon;
                cvda_ptr->prism = prism && (polygon > 2U);
                cvda_ptr->use_assigned_colors = use_assigned_colors;
                cvda_ptr->use_solid_color = use_solid_color;
                cvda_ptr->solid_color_r = solid_color_r;
                cvda_ptr->solid_color_g = solid_color_g;
                cvda_ptr->solid_color_b = solid_color_b;
                ncvda += 1U;
            }
        }
    }

    /* make sure there is something to do */
    if (status == 0 && cvdata.n == 0U) {
        usage("no vertex data files specified");
    }

    /* if all is well, display loaded point cloud */
    if (status == 0) {
        /* the following routine never returns */
        cview_gl_main_loop(argc,argv,&cvdata,
                           ncvda,cview_draw_arrays,
                           ncvline,cview_line);
    }

    /* all done */
    return (status == 0) ? 0 : 1;
}

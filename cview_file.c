/*
 * routines related to reading vertex data files
 *
 * Jeffrey Biesiadecki, 9/21/2019
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "kv_parse.h"
#include "cview_file.h"

/*-------------------------------------------------------------------------
 * callback function for parsing a single line of an XYZ file
 *
 * return 0 on success, -1 otherwise
 */

static int cview_file_parse_fn(kv_parse_t *kvp_ptr,
    int argc, char *argv[], void *client_data)
{
    int status = 0;
    cview_data_t *cvdata_ptr = (cview_data_t *)client_data;

    /* sanity check arguments from caller */
    assert(argv != NULL || argc <= 0);
    assert(cvdata_ptr != NULL);

    /* process this line, adding to cvdata_ptr */
    if (argc < 1) {
        kv_parse_error(kvp_ptr,
            "expected at least one word but only found %d",argc);
        status = -1;
    } else if (argc == 3) {
        double vx = 0.0;
        double vy = 0.0;
        double vz = 0.0;
        if (kv_parse_double(kvp_ptr,argv[0],&vx,0.0,0.0) != 0) {
            status = -1;
        } else if (kv_parse_double(kvp_ptr,argv[1],&vy,0.0,0.0) != 0) {
            status = -1;
        } else if (kv_parse_double(kvp_ptr,argv[2],&vz,0.0,0.0) != 0) {
            status = -1;
        } else {
            /* flip to OpenGL coordinates */
            status = cview_data_add_xyz(cvdata_ptr,vy,vx,-vz);
            if (status != 0) {
                kv_parse_error(kvp_ptr,
                    "too many vertices found, maximum is %u",cvdata_ptr->nmax);
            }
        }
    } else {
        kv_parse_error(kvp_ptr,"cview_file_parse_fn found bad input line");
        status = -1;
    }

    return status;
}

/*-------------------------------------------------------------------------
 * read a vertex data file, adding points and primitives to render
 *
 * cvdata_ptr must have been initialized with cview_data_init().
 * it is OK to read a series of vertex files after that initialization.
 *
 * return 0 on success, -1 otherwise
 */

int cview_file_read(cview_data_t *cvdata_ptr, const char *filename)
{
    int status = -1;

    assert(cvdata_ptr != NULL);
    assert(filename != NULL);

    kv_parse_t kvp;
    (void)kv_parse_init(&kvp,cview_file_parse_fn,cvdata_ptr);
    if (kv_parse_file(&kvp,filename) == 0) {
        status = 0;
    }
    kv_parse_clear(&kvp);

    return status;
}

# cview

This repo contains a simple point cloud viewer implemented in C99 and OpenGL.

## Dependencies

This program depends on old-school OpenGL (prior to 3.1), GLU, and GLUT.
A free software-based renderer can be installed on Ubuntu machines with:

```
% sudo apt-get install libglu1-mesa-dev freeglut3-dev mesa-common-dev
```

## Compiling and Running an Example

To compile, issue:

```
% make
```

This compiles the viewer program ``cview``,
as well as a library ``libcview.a`` to allow writing programs
that read vertex data from other file formats or generate vertex data.

Or, to compile and run an example point cloud with a couple line segments,
issue:

```
% make run
```

## Keyboard Mapping, When 3D View Window Has Focus

key     | description
--------|--------------------------------------------------------
F1/?    | get this help
HOME    | reset view back to initial view
a/A     | increase/decrease axis display, also turns on/off
c/C     | cycle center between line segment start and vertex mean
l/L     | increase/decrease line segment width
m/M     | increase/decrease line/plane rendering style
p/P     | increase/decrease point cloud vertex size
q/Q/ESC | quit program
w/W     | swap wire and fill frame rendering modes
z/Z     | move points towards/away from camera
LMB     | hold down to translate points and line segments
RMB     | hold down to rotate points and line segments
ARROWS  | translate points and line segments
PGUP/DN | rotate points and line segments CCW/CW

